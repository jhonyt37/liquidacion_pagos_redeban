package com.redeban;

import com.redeban.tipos.RegistroExcelGenerico;
import com.redeban.utils.Archivos;
import static com.redeban.utils.DEFINITIONS.HOJA_UNO;
import com.redeban.utils.RedebanLogger;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.JLabel;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import static com.redeban.utils.DEFINITIONS.XLSX_EXT;
import java.io.FileInputStream;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static org.apache.poi.hssf.usermodel.HSSFClientAnchor.MAX_COL;
import org.apache.poi.ss.usermodel.Font;

public final class ExcelSalida {

    String nombreArchivo = "";
    String rutaArchivo = "";
    JLabel lblSalida;
    XSSFWorkbook libro;
    XSSFSheet hojaActual;
    XSSFCell cell;
    short height = 400 * 2;
    XSSFRow row;
    private int indexToInsert;
    private static final int WIDTH_STANDAR = 15 * 400;

    RegistroExcelGenerico[] matriz;

    public ExcelSalida() {

    }

    public ExcelSalida(File input, String hoja) {
        String ruta = input.getParent() + "\\";
        String salida = input.getName().substring(0, input.getName().indexOf(XLSX_EXT));
        this.libro = new XSSFWorkbook();
        setFileName(salida, ruta, hoja);
        indexToInsert = 0;

        this.row = this.hojaActual.createRow(0);//se crea las filas
        this.row.setHeight(height);
        this.cell = row.createCell(0);//se crea las celdas para la cabecera, junto con la posición
    }

    public ExcelSalida(int size, String salida, String ruta, String hoja, JLabel lbl) {

        indexToInsert = 0;
        lblSalida = lbl;
        this.libro = new XSSFWorkbook();
        setMatrizSize(size);
        setFileName(salida, ruta, hoja);
        row = this.hojaActual.createRow(0);//se crea las filas
        this.row.setHeight(height);
        cell = row.createCell(0);//se crea las celdas para la cabecera, junto con la posición

    }

    void setMatrizSize(int lenFile) {
        this.matriz = new RegistroExcelGenerico[lenFile];
    }

    String[] obtenerStrings(Object... input) {
        String[] tmp = new String[MAX_COL];
        int size = 0;

        for (Object registro : input) {
            if (registro instanceof String[]) {
                String[] arr = (String[]) registro;
                for (String in : arr) {
                    tmp[size] = in;
                    size++;

                }
            } else if (registro instanceof String || registro instanceof Integer) {
                tmp[size] = (String) registro;
                size++;
            }

        }
        String[] out = new String[size];
        System.arraycopy(tmp, 0, out, 0, size);

        return out;

    }

    void addMatrizData(Object... input) {
        if (indexToInsert < this.matriz.length) {
            String[] data = obtenerStrings(input);
            this.matriz[indexToInsert] = new RegistroExcelGenerico(data.length);
            this.matriz[indexToInsert].setContenido(data);
            indexToInsert++;
        }
    }

    void setFileName(String salida, String ruta, String hoja) {
        this.nombreArchivo = salida + XLSX_EXT;
        this.rutaArchivo = ruta + nombreArchivo;
        this.hojaActual = this.libro.createSheet(hoja);
    }

    void agregaHoja(String nombre) {
        this.hojaActual = this.libro.createSheet(nombre);
    }

    private void addColumnHeader(CellStyle headerStyle, int indexCol, String nameCol) {
        cell = row.createCell(indexCol);//se crea las celdas para la cabecera, junto con la posición
        cell.setCellStyle(headerStyle); // se añade el style crea anteriormente 
        cell.setCellValue(nameCol);//se añade el contenido					
        cell.setCellValue(nameCol);//se añade el contenido					
    }

    public void crearHeaders(Object... nombreColumnas) {

        CellStyle headerStyle = libro.createCellStyle();
        
        Font headerFont = libro.createFont();
        
        headerFont.setColor(IndexedColors.WHITE.getIndex());
        headerStyle.setFont(headerFont);

        headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        headerStyle.setAlignment(HorizontalAlignment.CENTER);

        headerStyle.setWrapText(true);

        
        
        headerStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
        
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        lblSalida.setText("Almacenando: 0%");
        RedebanLogger.escribeLogInfo("Almacenando: 0%");
        //generar los datos para el documento

        row.setHeight(height);
        
        int indexCol = 0;
        String[] dataColumnas = obtenerStrings(nombreColumnas);
        for (String columna : dataColumnas) {
            if (columna != null) {
                addColumnHeader(headerStyle, indexCol++, columna);
            }
        }

    }

        public void setSizeCols(Object ...header) {
            int sizeHeader=0;
            for(Object obj:header){
                if(obj instanceof String[])
                    sizeHeader+=((String[]) obj).length;
                else if(obj instanceof String)
                    sizeHeader++;
            }
        for (int colum = 0; colum < sizeHeader; colum++) {
            hojaActual.autoSizeColumn(colum);
        }
    }
        
    public void setSizeCols(int columnas) {
        for (int colum = 0; colum < columnas; colum++) {
            hojaActual.setColumnWidth(colum, WIDTH_STANDAR);
        }
    }

    public void saveFile() throws IOException {
        File file;
        file = new File(rutaArchivo);
        File directorio = new File(rutaArchivo.substring(0, rutaArchivo.indexOf(nombreArchivo)));
        if (file.exists()) {// si el archivo existe se crea una copia
            Path pat = Paths.get(file.getAbsolutePath());
            Files.delete(pat);
            file = new File(rutaArchivo);
        }
        directorio.mkdirs();
        try (FileOutputStream fileOuS = new FileOutputStream(file)) {
            
            this.libro.write(fileOuS);
            this.libro.close();
            fileOuS.flush();

            RedebanLogger.escribeLogInfo("Archivo Creado");

        } catch (FileNotFoundException e) {
            RedebanLogger.escribeLogInfo("error archivo " + e.getMessage());
        } catch (IOException e) {
            RedebanLogger.escribeLogInfo("error archivo out " + e.getMessage());
        }
    }

    public void crearBody(int offSet) throws IOException {

        float steps = 100 / (float) matriz.length;

        for (int i = 0; i < matriz.length; i++) {
            try {
                RegistroExcelGenerico registro = matriz[i];

                lblSalida.setText("Almacenando: " + (int) (i * steps) + "%");
                RedebanLogger.escribeLogInfo("Almacenando: " + (int) (i * steps) + "%");
                row = hojaActual.createRow(i + offSet);//se crea las filas
                row.setHeight(height);
                int item = 0;

                if (registro != null) {
                    for (Object celda : registro.getContenido()) {

                        if ((celda instanceof String)) {
                            cell = row.createCell(item);//se crea las celdas para la contenido, junto con la posición
                            
                            cell.setCellValue(String.valueOf(celda)); //se añade el contenido

                        } else {
                            RedebanLogger.escribeLogInfo("error objeto no es String o arr String ");
                        }
                        item++;
                    }

                } else {
                    break;
                }

            } catch (Exception ex) {
                throw new IOException(ex);
            }
        }
    }

    public int leerArchivoPrevio(String r,String hoja) throws IOException {
        int res = 0;
        File myFile = new File(r);
        if (myFile.exists()) {
            try (FileInputStream inputStream = new FileInputStream(myFile)) {
                this.libro = new XSSFWorkbook(inputStream);
                
                this.hojaActual = this.libro.getSheet(hoja);
                res = this.hojaActual.getLastRowNum();
            }
        }
        return res;

    }

    public int obtenerExistente(boolean append) throws IOException {
        int offset = 0;
        if (append) {
            File fileInput = new File(rutaArchivo);
            if (fileInput.exists()) {
                offset = leerArchivoPrevio(rutaArchivo, HOJA_UNO) + 1;
            }

        }
        return offset;
    }
    
    public void procesarDatosExcel(boolean append, Object... header) throws IOException {
        if (matriz.length > 0) {
            int offset = obtenerExistente(append);
            
            if (offset == 0) {
                crearHeaders(header);
                offset++;
            }

            crearBody(offset);

            setSizeCols(header);
            lblSalida.setText("Almacenando: 99%... por favor espere");
            RedebanLogger.escribeLogInfo("Almacenando: 99%... por favor espere");

            saveFile();
        } else {
            RedebanLogger.escribeLogInfo("No hay registros de salida");
            if (!append && Archivos.deleteFile(rutaArchivo)) {
                RedebanLogger.escribeLogInfo("Archivo eliminado " + rutaArchivo);
            }
        }
    }
    
    public void crearExcelsalida(boolean append, Object... header) throws IOException {

        try {
            if (nombreArchivo != null && rutaArchivo != null) {
                procesarDatosExcel(append, header);
            } else {
                RedebanLogger.escribeLogInfo("error parametros no configurados");
            }
            lblSalida.setText("Almacenando: 100%");
            RedebanLogger.escribeLogInfo("Almacenando: 100%");
        } catch (IOException e) {
            throw new IOException(e);
        }
    }

}
