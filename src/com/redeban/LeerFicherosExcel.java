package com.redeban;

import com.redeban.utils.RedebanLogger;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JLabel;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
 
public class LeerFicherosExcel {

    private LeerFicherosExcel(){
        
    }
    
    private static void mostrarProgreso(JLabel lbl, int count) {
        if (lbl != null) {
            lbl.setText("Progreso: " + count);
        }

    }

    private static String obtenerContenido(CellType type, Cell cell, XSSFWorkbook worbook) {
        String res;
        if (type == CellType.STRING) {
            RedebanLogger.escribeLogInfo(cell.getStringCellValue() + " | ");
            res = cell.getStringCellValue();

            if (res.contains("%")) {
                BigDecimal d = new BigDecimal(res.trim().replace("%", "").replace(",", ".")).divide(BigDecimal.valueOf(100));
                res = d.toString();
            }

        } else if (type == CellType.NUMERIC) {
            // Using HSSFDateUtil to check if a cell contains a date.
            if (HSSFDateUtil.isCellDateFormatted(cell)) {
                RedebanLogger.escribeLogInfo("The cell contains a date value: "
                        + cell.getDateCellValue());

                res = String.valueOf(new SimpleDateFormat("dd/MM/yyyy").format(cell.getDateCellValue()));
            } else {

                if (cell.getNumericCellValue() >= 1 || cell.getNumericCellValue() == 0) {
                    RedebanLogger.escribeLogInfo(BigDecimal.valueOf(cell.getNumericCellValue()).toPlainString() + " | ");
                    BigDecimal value = BigDecimal.valueOf(cell.getNumericCellValue());
                    value = value.setScale(0, RoundingMode.UNNECESSARY);
                    res = String.valueOf(value);

                } else {
                    RedebanLogger.escribeLogInfo(cell.getNumericCellValue() + " | ");
                    res = String.valueOf(cell.getNumericCellValue());
                }

            }
        }else if (type == CellType.FORMULA) {
            FormulaEvaluator evaluator = worbook.getCreationHelper().createFormulaEvaluator();
            CellValue cellValue = evaluator.evaluate(cell);
            CellType typeF = cellValue.getCellTypeEnum();
            if (typeF == CellType.STRING) {
                RedebanLogger.escribeLogInfo(cellValue.getStringValue() + " | ");
                res = String.valueOf(cellValue.getStringValue());
            } else {
                RedebanLogger.escribeLogInfo(cellValue.getNumberValue() + " | ");
                res = String.valueOf(cellValue.getNumberValue());
            }
        } else {
            RedebanLogger.escribeLogError("Error formato de celda no valido");
            res = "";
        }
        return res;
    }

    private static List<String> procesarCeldas(boolean readContent, Iterator<Cell> cellIterator, XSSFWorkbook worbook, int count) {

        Cell cell;
        List<String> fila = new ArrayList<>();
        if (readContent) {
            while (cellIterator.hasNext()) {

                // se obtiene la celda en específico y se la imprime
                cell = cellIterator.next();
                CellType type = cell.getCellTypeEnum();

                RedebanLogger.escribeLogInfo(type.name() + " | ");

                //agrega contenido en formato String
                fila.add(obtenerContenido(type,cell,worbook));
            }
        } else {
            fila.add(count + "");
        }
        return fila;
    }

    public static Set<List<String>> leerArchivoExcel(File entrada, String hoja, int offset, int lines, boolean readContent, JLabel lbl) throws IOException {
        Set<List<String>> setDatos = Collections.synchronizedSet(new LinkedHashSet<>());
        RedebanLogger.escribeLogInfo("entra a leer excel");

        String rutaArchivo = "" + entrada.getAbsolutePath();

        try ( FileInputStream file = new FileInputStream(new File(rutaArchivo))) {
            // leer archivo excel
            XSSFWorkbook worbook = new XSSFWorkbook(file);
            //obtener la hoja que se va leer
            XSSFSheet sheet = worbook.getSheet(hoja);
            //obtener todas las filas de la hoja excel
            Iterator<Row> rowIterator = sheet.iterator();

            Row row;
            int count = 0;
            // se recorre cada fila hasta el final

            while (rowIterator.hasNext()) {

                mostrarProgreso(lbl, count);

                row = rowIterator.next();
                count++;
                //salta header
                if (count > offset) {

                    //se obtiene las celdas por fila
                    Iterator<Cell> cellIterator = row.cellIterator();

                    //se recorre cada celda
                    List<String> fila = procesarCeldas(readContent, cellIterator, worbook, count);

                    setDatos.add(fila);
                    //para lectura parcial
                    if ((count - offset) == lines && lines > 0) {
                        break;
                    }
                }
            }
        } catch (Exception e) {
            RedebanLogger.escribeLogInfo("error en entrada22 " + e.getMessage());
            throw new IOException(e.getMessage());

        }
        return setDatos;
    }

        
     
}