/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.redeban;

import com.redeban.utils.Zipper;
import com.redeban.utils.RedebanLogger;
import com.redeban.tipos.LlaveData;
import com.redeban.tipos.InfoRegistro;
import com.redeban.windows.MainWondow;
import static com.redeban.ExtractoInfo.COLUMNA_CAMPO_CUENTA;
import com.redeban.utils.Archivos;
import com.redeban.utils.ConfigRBM;
import com.redeban.utils.DEFINITIONS;
import static com.redeban.utils.DEFINITIONS.HOJA_UNO;
import com.redeban.utils.RBMUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import net.lingala.zip4j.exception.ZipException;

/**
 *
 * @author jmtoro
 */
public class ExtractoBanco extends Archivos{


    
    /**
     * @return the nuevoArchivo
     */
    public static File getNuevoArchivo() {
        return nuevoArchivo;
    }

    /**
     * @param aNuevoArchivo the nuevoArchivo to set
     */
    public static void setNuevoArchivo(File aNuevoArchivo) {
        nuevoArchivo = aNuevoArchivo;
    }

    
    private static final String RUTA_SALIDA_AUX="Descartados\\";
    private static final String RUTA_SALIDA_ALERTAS="Alertas\\";
    
    private static final String NOMBRE_SALIDA ="_procesados_OK";
    private static final String NOMBRE_SALIDA_AUX="_descartados";
    private static final String NOMBRE_SALIDA_ALERTAS="_alertas";
    
    private static File nuevoArchivo=null;
    private static int offset;
    private static String rutaSalida;
    private static String nombreSalida;
    private static ExtractoInfo extractoData;
    private static int entradas;
    private static int salidasOK;
    private static int descartadas;
    private static int duplicadas;
    private static int alertas;
    private static int previos;
        
        /**
     * @return the extractoData
     */
    public static ExtractoInfo getExtractoData() {
        return extractoData;
    }

    /**
     * @param aExtractoData the extractoData to set
     */
    public static void setExtractoData(ExtractoInfo aExtractoData) {
        extractoData = aExtractoData;
    }
    public static void init(JLabel lbl){
        setLabelOut(lbl);
    }
    private static void setPosTipoTxn() {
        
        getExtractoData().setIndexPosTipoTXN(ExtractoInfo.COLUMNA_CAMPO_TIPO_TRANSACCION);
        
    }
    
        private static void setLlaveBase(){
        
        getExtractoData().defineCamposLlaveBase(
            ExtractoInfo.COLUMNA_CAMPO_ULTIMOSCUATRO,
            ExtractoInfo.COLUMNA_CAMPO_CU,
            ExtractoInfo.COLUMNA_CAMPO_APROBACION,
            ExtractoInfo.COLUMNA_CAMPO_TOTAL
            );
        
        
    }
  
    public static void setSalida(String ruta, String nombreArchivo) {

        rutaSalida = ruta;
        nombreSalida = nombreArchivo;

    }
    
    public static String setEntradaEntidad(File inputFile, int off,boolean setFile) throws IOException {
        String res = null;
        if (setFile) {
            setNuevoArchivo(inputFile);
        }
        offset = off;

        if (inputFile != null) {
            try (FileReader fr = new FileReader(inputFile);
                    BufferedReader br = new BufferedReader(fr)) {
                if (getExtractoData() == null) {
                    RedebanLogger.escribeLogInfo("Agregando header de entrada Entidad ");
                    res = br.readLine();
                    setExtractoData(new ExtractoInfo(res, ';'));
                    setPosTipoTxn();
                    setLlaveBase();
                }
                else{
                    res = Arrays.toString(getExtractoData().getListaCampos()).replaceAll("\\[", "").replaceAll("\\]","").replaceAll(",", ";");
                }
            } catch (Exception ex) {
                throw new IOException(ex.getMessage());
            }
        }
        return res;
    }

    public static File extraeArchivo(File entradaFile) throws ZipException {
        File response = null;
        try {
            
            if (entradaFile.getName().substring(entradaFile.getName().indexOf(".")).equalsIgnoreCase(".zip")) {

                String pwd = "";
                Zipper zip = new Zipper();
                if (zip.checkPassword(entradaFile.getAbsolutePath())) {
                    pwd = JOptionPane.showInputDialog(null, "Debe digitar la clave del archivo:", "Archivo Cifrado", 0);
                }
                if (pwd != null) {
                    zip.unpack(entradaFile.getAbsolutePath(),
                            rutaSalida + entradaFile.getName().substring(0, entradaFile.getName().indexOf(".zip")),
                            pwd);

                    RedebanLogger.escribeLogInfo("Archivo descomprimido exitosamente: " + entradaFile.getName());
                    File ruta = new File(rutaSalida + entradaFile.getName().substring(0, entradaFile.getName().indexOf(".zip")));
                    JFileChooser chooser = new JFileChooser(ruta);
                    FileNameExtensionFilter filter = new FileNameExtensionFilter(
                            "CSV, Archivo CSV", "csv");
                    chooser.setFileFilter(filter);
                    int returnVal = chooser.showOpenDialog(null);
                    if (returnVal == JFileChooser.APPROVE_OPTION) {
                        response = chooser.getSelectedFile().getAbsoluteFile();
                    }
                }
            } else {
                response = entradaFile;
            }
        } catch (ZipException ex) {
            deleteDir(new File(rutaSalida + entradaFile.getName().substring(0, entradaFile.getName().indexOf(".zip"))));
            
            if (ex.getMessage().contains("Wrong Password?")) {
                throw new ZipException("Clave No valida o archivo de salida en uso:"
                        + "\n" + entradaFile.getName()
                        + "\n" + rutaSalida + entradaFile.getName().substring(0, entradaFile.getName().indexOf(".zip")));
            } else {
                throw new ZipException(ex);
            }
        }
        return response;
    }
    
    private static void deleteDir(File file) {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File f : contents) {
                if (!Files.isSymbolicLink(f.toPath())) {
                    deleteDir(f);
                }
            }
        }
        Path pat = Paths.get(file.getAbsolutePath());
        try {
            Files.delete(pat);
        } catch (IOException ex) {
            RedebanLogger.escribeLogError(ex.getLocalizedMessage());
        }
    }
    
    
    public static int procesoPendientes(File inputFile) throws IOException {

         int res=0;
        RedebanLogger.escribeLogInfo("carga de pendientes Entidad  ");
        if (inputFile != null) {
            try {

                String header = RBMUtils.obtenerHeader(inputFile,HOJA_UNO);
                int sizeFile = RBMUtils.sizeExistente(inputFile);
                setExtractoData(new ExtractoInfo(header, sizeFile, ';'));
                setPosTipoTxn();
                setLlaveBase();
                
                viewData("Registros: " + (sizeFile));
                RedebanLogger.escribeLogInfo("Progreso : 1% tamaño de archivo " + sizeFile + " registros");
                if( sizeFile == getExtractoData().incluirPendientes(inputFile,HOJA_UNO, 1)){
                    res=sizeFile;
                    RedebanLogger.escribeLogInfo("Registros pendientes importados con exito");
                    MainWondow.mostrarMensaje("IMPORTACION EXITOSA",
                            "Proceso de importar pendientes exitoso"
                            + "\n Registros incluidos: " + sizeFile
                            + "\n\n Para Consultar detalle vaya a   : " + inputFile.getAbsolutePath(), 1);
   
                }
                else
                    throw new IOException("Error en proceso de importar registros pendientes");

            } catch (IOException ex) {
                throw new IOException(ex.getMessage());
            }
        }
        return res;
    }
    
    public static void asignarRegistro(String line) throws IOException {

        InfoRegistro data;
        LlaveData llaveBase;
        llaveBase = getExtractoData().obtieneLlaveBase(line);

        if (llaveBase != null) {
            entradas++;
            int result = 0;
            if (1 == (result = getExtractoData().agegaRegistroBase(llaveBase, line))) {
                data = getExtractoData().obtieneRegistro(llaveBase);
                RedebanLogger.escribeLogInfo("Registro agregado/actualizado: " + Arrays.toString(data.getRegistro()));
            } else if (result == 2) {
                RedebanLogger.escribeLogInfo("registro no procesado");
            } else {
                duplicadas++;
                RedebanLogger.escribeLogInfo("registro duplicado");
            }
        } else {
            RedebanLogger.escribeLogInfo("Error registro no valido: -" + line + "-");
        }

    }

    public static void procesarContenido(BufferedReader br) throws IOException {
        float fileSize = countLinesFile(getNuevoArchivo());
        if (fileSize <= 0) {
            throw new IOException("Error archivo de entrada vacio");
        }
        viewData("Registros Extracto: " + (fileSize - 1));
        RedebanLogger.escribeLogInfo("Progreso : 1% tamaño de archivo " + fileSize + " registros");

        float steps = 100 / fileSize;
        RedebanLogger.escribeLogInfo("filesize  = " + fileSize + " " + steps);

        if (getExtractoData() == null) {
            throw new IOException("Error en inicializacion de entrada Entidad");
        }

        entradas = duplicadas = 0;
        int count = 0;
        String line;
        while ((line = br.readLine()) != null) {
            count++;
            if (count <= offset) {
                continue;
            }
            viewData("Procesando Extracto: " + (int) (steps * count) + " %");

            //elimna decimales y caracteres especiales
            if (ConfigRBM.getFactorMontosEntidad() != null) {
                line = line.replace(",00", "");
            }
            line = line.replaceAll("[^a-zA-Z0-9;/]", "");

            asignarRegistro(line);
        }
    }

    public static Set<String[]> procesaArchivo(boolean showStatus) throws IOException {

        
        Set<String[]> datosOk = null;
        final Long startHashSetTime = System.currentTimeMillis();
        previos = getExtractoData().getRegistrosBase().size();

        if (getNuevoArchivo() != null) {
            try ( FileReader fr = new FileReader(getNuevoArchivo());  BufferedReader br = new BufferedReader(fr)) {

                procesarContenido(br);

            } catch (Exception ex) {
                throw new IOException(ex.getMessage());
            }
        } else {
            entradas = 0;
        }
        datosOk = generaArchivos();
        final Long endHashSetTime = System.currentTimeMillis();
        RedebanLogger.escribeLogInfo("Time spent by HashMap: " + (endHashSetTime - startHashSetTime));

        mostrarResultado(showStatus);

        return datosOk;
    }

    private static void mostrarResultado(boolean showStatus) {
        String ultimaLinea = "";
        if (rutaSalida != null) {
            ultimaLinea = "\n\n Para Consultar detalle vaya a   : " + rutaSalida;
        }
        if (showStatus) {
            MainWondow.mostrarMensaje("Resultado Preproceso", "Resultado proceso: "
                    + "\n entrada Actual          : " + entradas
                    + "\n registros previos       : " + previos
                    + "\n salidasOK               : " + salidasOK
                    + "\n Anulaciones/Reversiones : " + descartadas
                    + "\n Alertas                 : " + alertas
                    + "\n Duplicados              : " + duplicadas
                    + ultimaLinea, 1);
        }
    }

    private static int obtieneCondicion(InfoRegistro dataEntrada) throws IOException {
        int res;
        boolean estadoCredito = dataEntrada.getEstadoIndex(ExtractoInfo.TIPO_CREDITO);
        boolean estadoDebito = dataEntrada.getEstadoIndex(ExtractoInfo.TIPO_DEBITO);
        String[] input = dataEntrada.getRegistro();

        String cuentas = ConfigRBM.getCuentasBancoOK();

        res = REGISTRO_ALERTA;
        for (String cta : cuentas.split(",")) {
            if (extractoData.filtarRegistros(COLUMNA_CAMPO_CUENTA, input, cta)) {
                res = REGISTRO_OK;
            }
        }

        if (estadoCredito && !estadoDebito) {
            res = REGISTRO_OK;
        }
        if (!estadoCredito && estadoDebito) {
            res = REGISTRO_OK;
        }
        if (estadoCredito && estadoDebito) {
            res = REGISTRO_DESCARTADO;
        }

        return res;
    }

    public static Set<String[]> generaArchivos() throws IOException {
        Set<String[]> response = null;
        try {
            

            int lenFile = getExtractoData().sizeData();

            response = new HashSet<>();
            ExcelSalida archivoOK = new ExcelSalida(lenFile, nombreSalida + NOMBRE_SALIDA, rutaSalida , HOJA_UNO, getLabelOut());
            ExcelSalida archivoDescartados = new ExcelSalida(lenFile, nombreSalida + NOMBRE_SALIDA_AUX, rutaSalida + RUTA_SALIDA_AUX, HOJA_UNO, getLabelOut());
            ExcelSalida archivoAlertas = new ExcelSalida(lenFile, nombreSalida + NOMBRE_SALIDA_ALERTAS, rutaSalida + RUTA_SALIDA_ALERTAS, HOJA_UNO, getLabelOut());
        
            descartadas=salidasOK=alertas=0;
            for (Map.Entry<LlaveData, InfoRegistro> entry : getExtractoData().getRegistrosBase().entrySet()) {
                int tipoSalida = obtieneCondicion(entry.getValue());
                switch (tipoSalida) {
                    case REGISTRO_OK:
                        salidasOK++;
                        response.add(entry.getValue().getRegistro());
                        archivoOK.addMatrizData(entry.getValue().getRegistro(), "OK");
                        break;
                    case REGISTRO_ALERTA:
                        alertas++;
                        archivoAlertas.addMatrizData(entry.getValue().getRegistro(), "Error combinacion no valida verifique origen");
                        break;
                    case REGISTRO_DESCARTADO:
                        descartadas++;
                        archivoDescartados.addMatrizData(entry.getValue().getRegistro(), "anulada/reversada");
                        break;
                    default: 
                        break;
                }
            }
            if (nombreSalida != null && rutaSalida != null) {
                archivoAlertas.crearExcelsalida(false,getExtractoData().getListaCampos(), DEFINITIONS.ETIQUETA_ESTADO);
                if (alertas > 0) {
                    int opc = JOptionPane.showConfirmDialog(null,
                            "Existen registros con inconsistencias CUENTA NO VALIDA"
                            + "\n\n Por favor verifique la entrada y ejecute nuevamente el proceso"
                            + "\n\n\n Desea continuar el proceso con estas inconsistencias?",
                            "Error Inconsisencias",
                            JOptionPane.OK_CANCEL_OPTION);
                    if (opc == 0) {
                        archivoOK.crearExcelsalida(false,getExtractoData().getListaCampos(), DEFINITIONS.ETIQUETA_ESTADO);
                        archivoDescartados.crearExcelsalida(false,getExtractoData().getListaCampos(), DEFINITIONS.ETIQUETA_ESTADO);
                    } else {
                        throw new IOException("Inconsistencias encontradas, verique entrada!!!. "
                                + "\n\nPara ver información detallada de las inconsistencias consulte:"
                                + "\n\n " + rutaSalida + RUTA_SALIDA_ALERTAS);
                    }
                } else {
                    archivoOK.crearExcelsalida(false,getExtractoData().getListaCampos(), DEFINITIONS.ETIQUETA_ESTADO);
                    archivoDescartados.crearExcelsalida(false,getExtractoData().getListaCampos(), DEFINITIONS.ETIQUETA_ESTADO);
                }
            }
        } catch (IOException ex) {
            throw new IOException(ex.getMessage());
        }
        return response;
    }
    
    
    
}