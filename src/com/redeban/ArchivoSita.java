/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.redeban;

import com.redeban.utils.RedebanLogger;
import com.redeban.tipos.LlaveData;
import com.redeban.windows.MainWondow;
import static com.redeban.SitaInfo.COLUMNA_CAMPO_BIN;
import static com.redeban.SitaInfo.COLUMNA_CAMPO_CU;
import static com.redeban.SitaInfo.COLUMNA_CAMPO_RECIBO;
import static com.redeban.SitaInfo.COLUMNA_CAMPO_RESPUESTA;
import static com.redeban.SitaInfo.COLUMNA_CAMPO_TIPO_TRANSACCION;
import static com.redeban.SitaInfo.COLUMNA_CAMPO_TOTAL;
import static com.redeban.SitaInfo.COLUMNA_CAMPO_ULTIMOSCUATRO;
import com.redeban.tipos.InfoRegistro;
import com.redeban.utils.Archivos;
import static com.redeban.utils.DEFINITIONS.ETIQUETA_ESTADO;
import static com.redeban.utils.DEFINITIONS.HOJA_UNO;
import com.redeban.utils.RBMUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author jmtoro
 */
public class ArchivoSita extends Archivos{


    /**
     * @return the descartadosMap
     */
    public static SitaInfo getDescartadosMap() {
        return descartadosMap;
    }

    /**
     * @param aDescartadosMap the descartadosMap to set
     */
    public static void setDescartadosMap(SitaInfo aDescartadosMap) {
        descartadosMap = aDescartadosMap;
    }

    /**
     * @return the nuevoArchivo
     */
    public static File getNuevoArchivo() {
        return nuevoArchivo;
    }

    /**
     * @param aNuevoArchivo the nuevoArchivo to set
     */
    public static void setNuevoArchivo(File aNuevoArchivo) {
        nuevoArchivo = aNuevoArchivo;
    }

    /**
     * @return the sitaData
     */
    public static SitaInfo getSitaData() {
        return sitaData;
    }

    /**
     * @param aSitaData the sitaData to set
     */
    public static void setSitaData(SitaInfo aSitaData) {
        sitaData = aSitaData;
    }
    
    private static SitaInfo descartadosMap;
    private static SitaInfo sitaData;
    private static String rutaSalida;
    private static String nombreSalida;
    private static File nuevoArchivo=null;
    private static int offset;
    private static int entradas;
    private static int salidasOK;
    private static int descartadas;
    private static int duplicadas;
    private static int otros;
    private static int alertas;
    private static int declinadas;
    private static int previos;
    
    
    
    private static final String RUTA_SALIDA_AUX="Descartados\\";
    private static final String RUTA_SALIDA_ALERTAS="Alertas\\";
    
    private static final String NOMBRE_SALIDA ="_procesados_OK";
    private static final String NOMBRE_SALIDA_AUX="_descartados";
    private static final String NOMBRE_SALIDA_ALERTAS="_alertas";
    
      

    public static void init(JLabel lbl, char separator) {
        setLabelOut(lbl);
        previos=otros=declinadas=duplicadas=entradas=descartadas=alertas=salidasOK=0;
        setCharSeparator(separator);
        
    }
    public static void setSalida(String ruta, String nombreArchivo) {

        rutaSalida = ruta;
        nombreSalida = nombreArchivo;

    }

    private static void setPosTipoTxn() {
        
        getSitaData().setIndexPosTipoTXN(COLUMNA_CAMPO_TIPO_TRANSACCION);
        
    }
    
    public static void setPreviaDataSita(Set<String[]> data) throws IOException {
        Map<LlaveData, InfoRegistro> sitaMap = RBMUtils.crearMap(data,
                COLUMNA_CAMPO_BIN,
                COLUMNA_CAMPO_ULTIMOSCUATRO,
                COLUMNA_CAMPO_CU,
                COLUMNA_CAMPO_TOTAL,
                COLUMNA_CAMPO_RECIBO);
        sitaData.setRegistrosBase(sitaMap);
    }
    
    
        private static void setLlaveBase(){
        
        getSitaData().defineCamposLlaveBase(COLUMNA_CAMPO_BIN,
            COLUMNA_CAMPO_ULTIMOSCUATRO,
            COLUMNA_CAMPO_CU,
            COLUMNA_CAMPO_TOTAL,
            COLUMNA_CAMPO_RECIBO);

    }


    public static void procesarLinea(String line) throws IOException {

        InfoRegistro data;
        LlaveData llaveBase;

        llaveBase = getSitaData().obtieneLlaveBase(line);

        if (llaveBase != null) {
            entradas++;
            if (!sitaData.filtarRegistros(COLUMNA_CAMPO_RESPUESTA, line, "APROBADA")) {
                declinadas++;
                int result = getDescartadosMap().agegaRegistroBaseFinal(llaveBase, line, 0);
                if (result != 1) {
                    RedebanLogger.escribeLogInfo("Registro descartado: " + result);
                }
            } else {
                int result = 0;
                if (1 == (result = getSitaData().agegaRegistroBase(llaveBase, line))) {
                    data = getSitaData().obtieneRegistro(llaveBase);
                    RedebanLogger.escribeLogInfo("Registro agregado/actualizado: " + Arrays.toString(data.getRegistro()));
                } else if (result == 2) {
                    otros++;
                    getDescartadosMap().agegaRegistroBaseFinal(llaveBase, line, 0);
                    RedebanLogger.escribeLogInfo("registro no procesado");
                } else {
                    duplicadas++;
                    RedebanLogger.escribeLogInfo("registro duplicado");
                }
            }
        } else {
            RedebanLogger.escribeLogInfo("Error registro no valido: -" + line + "-");
        }

    }

    public static void nuevoProceso() throws IOException {

        String line;
        float fileSize = countLinesFile(getNuevoArchivo());
        if (fileSize <= 0) {
            throw new IOException("Error archivo de entrada vacio");
        }
        float steps = 100 / fileSize;
        RedebanLogger.escribeLogInfo("filesize  = " + fileSize + " " + steps);

        int count = 0;
        try ( FileReader fr = new FileReader(getNuevoArchivo());  BufferedReader br = new BufferedReader(fr)) {

            viewData("Registros: " + (fileSize - 1));
            RedebanLogger.escribeLogInfo("Progreso : 1% tamaño de archivo " + fileSize + " registros");

            if (getSitaData() == null) {
                throw new IOException("Error en inicializacion de entrada");
            }
            while ((line = br.readLine()) != null) {
                count++;
                if (count <= offset) {
                    continue;
                }

                viewData("Procesando Sita: " + (int) (steps * count) + " %");
                
                procesarLinea(line);

                
            }
        } catch (Exception ex) {
            throw new IOException(ex.getMessage());
        }

    }

    public static Set<String[]> ejecutarProcesoSita(boolean showStatus) throws IOException {

        
        final Long startHashSetTime = System.currentTimeMillis();

        previos = getSitaData().getRegistrosBase().size();

        if (getNuevoArchivo() != null) {
            
            nuevoProceso();

        }
        else{
            entradas=0;
        }
        Set<String[]> datosOk = generaArchivos();
        final Long endHashSetTime = System.currentTimeMillis();
        
        RedebanLogger.escribeLogInfo("Time spent by HashMap: " + (endHashSetTime - startHashSetTime));
        String ultimaLinea = "";
        if (rutaSalida != null) {
            ultimaLinea = "\n\n Para Consultar detalle vaya a   : " + rutaSalida;
        }
        if(showStatus){
        MainWondow.mostrarMensaje("RESULTADO DEL PROCESO", "Resultado proceso: "
                + "\n entrada Actual          : " + entradas
                + "\n registros previos       : " + previos
                + "\n Declinadas              : " + declinadas
                + "\n Otras TXN               : " + otros
                + "\n salidasOK               : " + salidasOK
                + "\n Anulaciones/Reversiones : " + descartadas
                + "\n Alertas                 : " + alertas
                + "\n Duplicados              : " + duplicadas
                    + ultimaLinea, 1);
            RedebanLogger.escribeLogInfo("Preconcilia SITA        : ");
            RedebanLogger.escribeLogInfo("entrada Actual          : " + entradas);
            RedebanLogger.escribeLogInfo("registros previos       : " + previos);
            RedebanLogger.escribeLogInfo("Declinadas              : " + declinadas);
            RedebanLogger.escribeLogInfo("Otras TXN               : " + otros);
            RedebanLogger.escribeLogInfo("salidasOK               : " + salidasOK);
            RedebanLogger.escribeLogInfo("Anulaciones/Reversiones : " + descartadas);
            RedebanLogger.escribeLogInfo("Alertas                 : " + alertas);
            RedebanLogger.escribeLogInfo("Duplicados              : " + duplicadas);

        }

        return datosOk;
    }

    public static String setEntradaSita(File inputFile, int off,boolean setFile) throws IOException {
        if(setFile)
            setNuevoArchivo(inputFile);
        offset = off;
        String res=null;
        
        if (inputFile != null) {
            try (FileReader fr = new FileReader(inputFile);
                    BufferedReader br = new BufferedReader(fr)) {

                if (getSitaData() == null) {
                    RedebanLogger.escribeLogInfo("Agregando header de entrada SITA ");
                    res = br.readLine();
                    setSitaData(new SitaInfo(res, getCharSeparator()));
                    
                    setDescartadosMap(new SitaInfo(res, getCharSeparator()));
                    
                    setPosTipoTxn();
                    setLlaveBase();
                }
                else{
                    res = Arrays.toString(getSitaData().getListaCampos()).replaceAll("\\[", "").replaceAll("\\]","").replaceAll(",", ";");
                }

            } catch (Exception ex) {
                throw new IOException(ex.getMessage());
            }
        }
        return res;

    }
    
    
    
    
     public static int procesoPendientes(File inputFile) throws IOException {

         int res=0;
        RedebanLogger.escribeLogInfo("carga de pendientes SITA  ");
        if (inputFile != null) {
            try {

                String header = RBMUtils.obtenerHeader(inputFile,HOJA_UNO);
                int sizeFile = RBMUtils.sizeExistente(inputFile);
                setSitaData(new SitaInfo(header, sizeFile, ';'));
                setPosTipoTxn();
                setLlaveBase();
                
                viewData("Registros: " + (sizeFile));
                RedebanLogger.escribeLogInfo("Progreso : 1% tamaño de archivo " + sizeFile + " registros");
                if( sizeFile == getSitaData().incluirPendientes(inputFile,HOJA_UNO, 1)){
                    res=sizeFile;
                    RedebanLogger.escribeLogInfo("Registros pendientes importados con exito");
                    MainWondow.mostrarMensaje("IMPORTACION EXITOSA",
                            "Proceso de importar pendientes exitoso"
                            + "\n Registros incluidos: " + sizeFile
                            + "\n\n Para Consultar detalle vaya a   : " + inputFile.getAbsolutePath(), 1);
   
                }
                else
                    throw new IOException("Error en proceso de importar registros pendientes: cantidad de registros no coinciden");

            } catch (IOException ex) {
                throw new IOException(ex.getMessage());
            }
        }
        return res;
    }



     //Tabla de verdad dle proceso de prcociliacin Sita
    private static int obtieneCondicion(InfoRegistro dataEntrada){
        int res;
        boolean compra = dataEntrada.getEstadoIndex(SitaInfo.TIPO_COMPRA);
        boolean anula = dataEntrada.getEstadoIndex(SitaInfo.TIPO_ANULACION);
        boolean reverso = dataEntrada.getEstadoIndex(SitaInfo.TIPO_REVERSO_COMPRA);
        boolean anulaReverso = dataEntrada.getEstadoIndex(SitaInfo.TIPO_REVERSO_ANULACION);
        
        if(compra && !reverso && !anula && !anulaReverso)
            res=REGISTRO_OK;
        else if(!compra && !reverso && anula && !anulaReverso)
            res=REGISTRO_OK;
        else if(compra && !reverso && anula && anulaReverso)
            res=REGISTRO_OK;
        else if(compra && reverso && !anula && !anulaReverso)
            res=REGISTRO_DESCARTADO;
        else
            res=REGISTRO_ALERTA;

        if(compra && !reverso && anula && !anulaReverso)
            res=REGISTRO_DESCARTADO;        
        if(!compra && !reverso && anula && anulaReverso)
            res=REGISTRO_DESCARTADO;
        
        return res;
    }
    
    public static Set<String[]> asignarRegistros(int lenFile, ExcelSalida archivoOK, ExcelSalida archivoAlertas,ExcelSalida archivoDescartados) {
        
        HashSet<String[]> response = new HashSet<>();
        float steps = 100 / (float) lenFile;
        int count = 0;
        for (Map.Entry<LlaveData, InfoRegistro> entry : getSitaData().getRegistrosBase().entrySet()) {
            count++;
            viewData("Actualizando memoria: " + steps * count);
            RedebanLogger.escribeLogInfo("Actualizando memoria: " + steps * count);
            if (entry.getValue().getEstados() == null) {
                int tipo = 0;
                tipo = new SitaInfo().getTipo(Arrays.toString(entry.getValue().getRegistro()));
                boolean [] estados = new boolean[SitaInfo.getMAXESTADOS()];
                entry.getValue().setEstados(estados);
                entry.getValue().setEstadoIndex(tipo, true);

            }
            int tipoSalida = obtieneCondicion(entry.getValue());
            switch (tipoSalida) {
                case REGISTRO_OK:
                    salidasOK++;
                    archivoOK.addMatrizData((Object[])entry.getValue().getRegistro());
                    response.add(entry.getValue().getRegistro());
                    break;
                case REGISTRO_ALERTA:
                    alertas++;
                    archivoAlertas.addMatrizData(entry.getValue().getRegistro(), "Error combinacion no valida verifique origen");
                    break;
                case REGISTRO_DESCARTADO:
                    descartadas++;
                    archivoDescartados.addMatrizData(entry.getValue().getRegistro(), "anulada/reversada");
                    break;
                default:
                    break;
            }
        }
        
        if (getDescartadosMap() != null) {
            for (Map.Entry<LlaveData, InfoRegistro> entry : getDescartadosMap().getRegistrosBase().entrySet()) {
                archivoDescartados.addMatrizData(entry.getValue().getRegistro(), "declinada/otra txn");
            }
        }
        return response;
        
    }

    public static void grabarArchivos(ExcelSalida archivoOK, ExcelSalida archivoAlertas, ExcelSalida archivoDescartados) throws IOException {
        if (nombreSalida != null && rutaSalida != null) {

            archivoAlertas.crearExcelsalida(false, getSitaData().getListaCampos(), ETIQUETA_ESTADO);
            if (alertas > 0) {
                int opc = JOptionPane.showConfirmDialog(null,
                        "Existen registros con inconsistencias"
                        + "\n\n Por favor verifique la entrada y ejecute nuevamente el proceso"
                        + "\n\n\n Desea continuar el proceso con estas inconsistencias?",
                        "Error Inconsisencias",
                        JOptionPane.OK_CANCEL_OPTION);
                if (opc == 0) {
                    archivoOK.crearExcelsalida(false, (Object[])getSitaData().getListaCampos());
                    archivoDescartados.crearExcelsalida(false, getSitaData().getListaCampos(), ETIQUETA_ESTADO);
                } else {
                    throw new IOException("Inconsistencias encontradas, verique entrada!!!. "
                            + "\n\nPara ver información detallada de las inconsistencias consulte:"
                            + "\n\n " + rutaSalida + RUTA_SALIDA_ALERTAS);
                }
            } else {
                archivoOK.crearExcelsalida(false, (Object[])getSitaData().getListaCampos());
                archivoDescartados.crearExcelsalida(false, getSitaData().getListaCampos(), ETIQUETA_ESTADO);
            }
        }

    }

    public static Set<String[]> generaArchivos() throws IOException {
        Set<String[]> response =null;
        try {

            int lenFile = getSitaData().sizeData();
            if(entradas>lenFile)
                lenFile=entradas;
            
            ExcelSalida archivoOK = new ExcelSalida(lenFile, nombreSalida + NOMBRE_SALIDA, rutaSalida , HOJA_UNO, getLabelOut());
            ExcelSalida archivoDescartados = new ExcelSalida(lenFile, nombreSalida + NOMBRE_SALIDA_AUX, rutaSalida + RUTA_SALIDA_AUX, HOJA_UNO, getLabelOut());
            ExcelSalida archivoAlertas = new ExcelSalida(lenFile, nombreSalida + NOMBRE_SALIDA_ALERTAS, rutaSalida + RUTA_SALIDA_ALERTAS, HOJA_UNO, getLabelOut());
        
            descartadas=salidasOK=alertas=0;
            
            response = asignarRegistros(lenFile,archivoOK,archivoAlertas,archivoDescartados);
            
            grabarArchivos(archivoOK,archivoDescartados,archivoAlertas);
        
        } catch (IOException ex) {
            throw new IOException(ex.getMessage());
        }
        return response;
    }

}

