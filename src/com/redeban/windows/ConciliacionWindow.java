/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.redeban.windows;

import com.redeban.ArchivoSita;
import com.redeban.Conciliacion;
import com.redeban.ExtractoBanco;
import com.redeban.ExtractoInfo;
import com.redeban.utils.RedebanLogger;
import com.redeban.SitaInfo;
import com.redeban.utils.ConfigRBM;
import static com.redeban.utils.DEFINITIONS.BANCO_PROCESO_GUARDAR_TEMPORAL;
import static com.redeban.utils.DEFINITIONS.BANCO_PROCESO_NUEVO;
import static com.redeban.utils.DEFINITIONS.BANCO_PROCESO_PENDIENTES;
import static com.redeban.utils.DEFINITIONS.ETIQUETA_ERROR_SITA;
import static com.redeban.utils.DEFINITIONS.ETIQUETA_MEMORIA_CERO;
import static com.redeban.utils.DEFINITIONS.ETIQUETA_PENDIENTES;
import static com.redeban.utils.DEFINITIONS.ETIQUETA_SIN_PENDIENTES;
import static com.redeban.utils.DEFINITIONS.PROYECT_NAME;

import static com.redeban.utils.DEFINITIONS.SITA_PROCESO_GUARDAR_TEMPORAL;
import static com.redeban.utils.DEFINITIONS.SITA_PROCESO_NUEVO;
import static com.redeban.utils.DEFINITIONS.SITA_PROCESO_PENDIENTES;

import com.redeban.utils.RBMUtils;
import static com.redeban.windows.MainWondow.mostrarMensaje;
import java.awt.Color;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import net.lingala.zip4j.exception.ZipException;

/**
 *
 * @author jmtoro
 */
public final class ConciliacionWindow extends javax.swing.JFrame {
    
    static final long serialVersionUID = 46L;

    /**
     * @return the principal
     */
    public JFrame getPrincipal() {
        return principal;
    }

    /**
     * @param principal the principal to set
     */
    public void setPrincipal(JFrame principal) {
        this.principal = principal;
    }

    /**
     * @return the headerConcilia
     */
    public String getHeaderConcilia() {
        return headerConcilia;
    }

    /**
     * @param headerConcilia the headerConcilia to set
     */
    public void setHeaderConcilia(String headerConcilia) {
        this.headerConcilia = headerConcilia;
    }

    /**
     * @return the headerSita
     */
    public String getHeaderSita() {
        return headerSita;
    }

    /**
     * @param headerSita the headerSita to set
     */
    public void setHeaderSita(String headerSita) {
        this.headerSita = headerSita;
    }

    /**
     * @return the headerEntidad
     */
    public String getHeaderEntidad() {
        return headerEntidad;
    }

    /**
     * @param headerEntidad the headerEntidad to set
     */
    public void setHeaderEntidad(String headerEntidad) {
        this.headerEntidad = headerEntidad;
    }
    
    
    AuxArchivosWindow  aux=null;
    private String nombreSita=null;
    private String nombreExtracto=null;
    private String nombreConcilia=null;
    private String rutaSita=null;
    private String rutaExtracto=null;
    private String rutaConcilia=null;
    private String headerConcilia=null;
    private String headerSita=null;
    private String headerEntidad=null;
    private JFrame principal;
    
    HiloProcesos procesoSita;
    
    HiloProcesos procesoEntidad;
    Conciliacion procesoConciliacion;
    HiloConcilia procConcilia;

    /**
     * Creates new form Parametros
     */
    public ConciliacionWindow() {
        initComponents();
        setLocationRelativeTo(null);

        setTitle("Liquidacion_Pagos_RBM");
        try {
            String iconoString = ConfigRBM.getRutaProyecto() + "/recursos/imagenes/icono.png";
            RedebanLogger.escribeLogInfo("ruta icono: " + iconoString);
            File pathToFile = new File(iconoString);
            Image image = ImageIO.read(pathToFile);
            setIconImage(image);
        } catch (IOException ex) {
            RedebanLogger.escribeLogError(ex.getMessage());
        }
        btnGuardaSita.setEnabled(false);
        btnGuardaEntidad.setEnabled(false);
        btnCociliacion.setEnabled(false);
        chkSita.setEnabled(false);
        chkEntidad.setEnabled(false);
        chkHistoriaconcilia.setSelected(true);
        
    }
    public void setSitaReady(){
        
        chkSita.setSelected(true);
        chkSita.setBackground(Color.green);
        lblPreparadoSita.setBackground(Color.green);
        pnlSita.setBackground(Color.green);
        checkConcilia();
        
    }
    
    public void setEntidadReady(){
        
        chkEntidad.setSelected(true);
        chkEntidad.setBackground(Color.green);
        lblPreparadoentidad.setBackground(Color.green);
        pnlEntidad.setBackground(Color.green);
        checkConcilia();
        
    }
    
    public void checkConcilia(){
        if(chkEntidad.isSelected() && chkSita.isSelected()){
            btnCociliacion.setEnabled(true);
        }
}

    
    
        private class HiloProcesos extends Thread {

        /**
         * @return the show
         */
        public boolean isShow() {
            return show;
        }

        /**
         * @param show the show to set
         */
        public void setShow(boolean show) {
            this.show = show;
        }

        /**
         * @return the estado
         */
        public boolean isEstado() {
            return estado;
        }

        /**
         * @param estado the estado to set
         */
        public void setEstado(boolean estado) {
            this.estado = estado;
        }

        private int idTipoProceso;
        private File entradaFile;
        
        JLabel lblProgreso;
        private String rutaDestino;
        private String nameOut;
        private boolean estado=false;
        private boolean show=false;
        private Set<String[]> datosRta;
        
        
        HiloProcesos(File f){
            entradaFile = f;
        }
        
        HiloProcesos(int idTipo, File f, JLabel lbl) {
            this.entradaFile = f;
            this.lblProgreso = lbl;
            this.idTipoProceso = idTipo;
        }

        HiloProcesos(int idTipo, File f, JLabel lbl,String rd, String n) {
            this.entradaFile = f;
            this.lblProgreso = lbl;
            this.rutaDestino =rd;
            this.nameOut = n;
            this.idTipoProceso = idTipo;
        }
        
        HiloProcesos(File f, JLabel lbl) {
            entradaFile = f;
            lblProgreso = lbl;
            rutaDestino ="";
            
        }
                /**
         * @return the datosRta
         */
        public Set<String[]> getDatosRta() {
            return datosRta;
        }

        /**
         * @param datosRta the datosRta to set
         */
        public void setDatosRta(Set<String[]> datosRta) {
            this.datosRta = datosRta;
        }
        
        public void addDatosRta(Set<String[]> datosRta) {
               if (this.datosRta == null) {
                setDatosRta(datosRta);
            } else {
                this.datosRta.addAll(datosRta);
            }
        }

        @Override
        public void run() {

            try {

                int size=0;
                switch (this.idTipoProceso) {
                    case SITA_PROCESO_NUEVO:
                        ArchivoSita.init(lblProgreso,';');
                        setHeaderSita(ArchivoSita.setEntradaSita(entradaFile,1,true));
                        ArchivoSita.setSalida(rutaDestino,nameOut);
                        ArchivoSita.setPreviaDataSita(getDatosRta());
                        addDatosRta(ArchivoSita.ejecutarProcesoSita(isShow()));
                        
                        lblSitaActual.setText("<html><div style='text-align: center;'>En memoria:</div><br><div style='text-align: center;'>"+getDatosRta().size()+"</div></html>");
                        
                        btnGuardaSita.setEnabled(true);
                        setSitaReady();
                        this.setEstado(true);
                        lblSitaActual.setBackground(Color.green);
                        break;

                    case SITA_PROCESO_PENDIENTES:
                        lblPreparadoSita.setBackground(Color.orange);
                        ArchivoSita.init(lblProgreso,';');
                        
                        ArchivoSita.setSalida(rutaDestino,nameOut);
                        size= ArchivoSita.procesoPendientes(entradaFile);
                        if (size > 0) {
                            setHeaderSita(ArchivoSita.setEntradaSita(entradaFile,1,false));
                            addDatosRta(ArchivoSita.ejecutarProcesoSita(isShow()));
                            lblPendientesSita.setText(ETIQUETA_PENDIENTES + size);
                            this.setEstado(true);
                            setSitaReady();
                        }
                        lblSitaActual.setBackground(Color.gray);
                        break;
                    case SITA_PROCESO_GUARDAR_TEMPORAL:
                        
                        lblPreparadoSita.setBackground(Color.orange);
                        ArchivoSita.init(lblProgreso,';');
                        procesoConciliacion = new Conciliacion();
                        procesoConciliacion.init(lblProgreso);
                        procesoConciliacion.setSalida(nameOut, rutaDestino);
                        procesoConciliacion.generaArchivoSalida(getDatosRta(),"SITA",getHeaderSita());
                        lblSitaActual.setText(ETIQUETA_MEMORIA_CERO);
                        lblPendientesSita.setText(ETIQUETA_PENDIENTES + getDatosRta().size());
                        btnGuardaSita.setEnabled(false);
                        this.setEstado(true);
                        lblSitaActual.setBackground(Color.gray);
                        break;

                    case BANCO_PROCESO_NUEVO:
                        ExtractoBanco.init(lblProgreso);
                        ExtractoBanco.setSalida(rutaDestino, nameOut);
                        File f = ExtractoBanco.extraeArchivo(entradaFile);
                        setHeaderEntidad(ExtractoBanco.setEntradaEntidad(f,1,true));
                        addDatosRta(ExtractoBanco.procesaArchivo(isShow()));
                        lblEntidadActual.setText("<html><div style='text-align: center;'>En memoria:</div><br><div style='text-align: center;'>"+getDatosRta().size()+"</div></html>");
                        btnGuardaEntidad.setEnabled(true);
                        setEntidadReady();
                        lblEntidadActual.setBackground(Color.green);
                        this.setEstado(true);
                        break;
                    case BANCO_PROCESO_PENDIENTES:
                        lblPreparadoentidad.setBackground(Color.orange);
                        ExtractoBanco.init(lblProgreso);
                        ExtractoBanco.setSalida(rutaDestino, nameOut);
                        size = ExtractoBanco.procesoPendientes(entradaFile);
                        if (size > 0) {
                            setHeaderEntidad(ExtractoBanco.setEntradaEntidad(entradaFile,1,false));
                            addDatosRta(ExtractoBanco.procesaArchivo(isShow()));
                            lblPendientesEntidad.setText(ETIQUETA_PENDIENTES + size);
                            this.setEstado(true);
                            setEntidadReady();
                        }
                        lblEntidadActual.setBackground(Color.gray);
                        break;
                    case BANCO_PROCESO_GUARDAR_TEMPORAL:
                        lblPreparadoentidad.setBackground(Color.orange);
                        ExtractoBanco.init(lblProgreso);
                        procesoConciliacion = new Conciliacion();
                        procesoConciliacion.init(lblProgreso);
                        procesoConciliacion.setSalida(nameOut, rutaDestino);
                        procesoConciliacion.generaArchivoSalida(getDatosRta(),"Entidad",getHeaderEntidad());
                        lblEntidadActual.setText(ETIQUETA_MEMORIA_CERO);
                        lblEntidadActual.setBackground(Color.gray);
                        btnGuardaEntidad.setEnabled(false);
                        this.setEstado(true);
                        break;
                    default:
                        JOptionPane.showMessageDialog(null, "Error en archivo de entrada seleccionado xlsx");
                        throw new IOException("error en proceso seleccionado");

                }
            } catch (IOException | ZipException ex) {
                mostrarMensaje("Error en datos", "Error en proceso de compensacion: \n" + ex.getMessage(), JOptionPane.ERROR_MESSAGE);
                RedebanLogger.escribeLogError(ex.getMessage());
            }
        }
        
        

    }

    public void setInitValues() throws IOException {
       
        initWindow();
    }
    
    private void detenerProcesos(){
        //TODO implemntar logica que detenga cada proceso en ejecucion
        
    }
    
    public void cargarPendientes(boolean show) {
        
        try {
            String ruta= ConfigRBM.getRutaPendientesSita();
            String name = ConfigRBM.getNombrePendientesSita();
            File inputFile = new File(ruta+name+".xlsx");
            
            if (inputFile.exists()) {

                procesoSita = new HiloProcesos(SITA_PROCESO_PENDIENTES, inputFile.getAbsoluteFile(), lblProgreso);
                procesoSita.setShow(show);
                lblSitaActual.setBackground(Color.orange);
                procesoSita.start();
            }

            ruta = ConfigRBM.getRutaPendientesEntidad();
            name = ConfigRBM.getNombrePendientesEntidad();
            inputFile = new File(ruta + name + ".xlsx");

            if (inputFile.exists()) {

                procesoEntidad = new HiloProcesos(BANCO_PROCESO_PENDIENTES, inputFile.getAbsoluteFile(), lblProgreso);
                procesoEntidad.start();
                lblEntidadActual.setBackground(Color.orange);
            }
            
            
            
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Error Archivo Pendientes", ERROR_MESSAGE);
        }
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btnSita = new javax.swing.JButton();
        btnExtracto = new javax.swing.JButton();
        btnCociliacion = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        lblProgreso = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        btnSalidaConcilia = new javax.swing.JButton();
        btnPend1 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        btnGuardaEntidad = new javax.swing.JButton();
        btnGuardaSita = new javax.swing.JButton();
        lblPendientesSita = new javax.swing.JLabel();
        lblPendientesEntidad = new javax.swing.JLabel();
        lblSitaActual = new javax.swing.JLabel();
        pnlSita = new javax.swing.JPanel();
        chkSita = new javax.swing.JCheckBox();
        lblPreparadoSita = new javax.swing.JLabel();
        pnlEntidad = new javax.swing.JPanel();
        lblPreparadoentidad = new javax.swing.JLabel();
        chkEntidad = new javax.swing.JCheckBox();
        lblEntidadActual = new javax.swing.JLabel();
        chkHistoriaconcilia = new javax.swing.JCheckBox();
        btnStop = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 204, 204));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Proceso de conciliacion");

        btnSita.setBackground(new java.awt.Color(153, 255, 255));
        btnSita.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnSita.setText("Cargar Movimiento SITA");
        btnSita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSitaActionPerformed(evt);
            }
        });

        btnExtracto.setBackground(new java.awt.Color(153, 255, 255));
        btnExtracto.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnExtracto.setText("Cargar Extracto entidad");
        btnExtracto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExtractoActionPerformed(evt);
            }
        });

        btnCociliacion.setBackground(new java.awt.Color(153, 255, 153));
        btnCociliacion.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnCociliacion.setText("Conciliacion");
        btnCociliacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCociliacionActionPerformed(evt);
            }
        });

        jButton1.setText("Volver");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        lblProgreso.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lblProgreso.setText("Estado:");

        jButton2.setText("<html> Cambiar <br> Salida </html>");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("<html>Cambiar<br>Salida</html>");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Reestablecer valores");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        btnSalidaConcilia.setText("Cambiar Salida");
        btnSalidaConcilia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalidaConciliaActionPerformed(evt);
            }
        });

        btnPend1.setText("<html> Cargar <br> Pendientes </html>");
        btnPend1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPend1ActionPerformed(evt);
            }
        });

        jButton6.setText("<html>Cargar<br> Pendientes</html>");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        btnGuardaEntidad.setText("Guardar");
        btnGuardaEntidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardaEntidadActionPerformed(evt);
            }
        });

        btnGuardaSita.setText("Guardar");
        btnGuardaSita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardaSitaActionPerformed(evt);
            }
        });

        lblPendientesSita.setText("Registros pendientes: ");

        lblPendientesEntidad.setText("Registros pendientes: ");

        lblSitaActual.setBackground(new java.awt.Color(255, 204, 204));
        lblSitaActual.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblSitaActual.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSitaActual.setText("En memoria: 0");
        lblSitaActual.setOpaque(true);

        pnlSita.setBackground(new java.awt.Color(255, 51, 51));

        chkSita.setBackground(new java.awt.Color(255, 51, 51));

        lblPreparadoSita.setBackground(new java.awt.Color(255, 51, 51));
        lblPreparadoSita.setText("Preparado para conciliar");
        lblPreparadoSita.setOpaque(true);

        javax.swing.GroupLayout pnlSitaLayout = new javax.swing.GroupLayout(pnlSita);
        pnlSita.setLayout(pnlSitaLayout);
        pnlSitaLayout.setHorizontalGroup(
            pnlSitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlSitaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(chkSita)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblPreparadoSita, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlSitaLayout.setVerticalGroup(
            pnlSitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(chkSita, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnlSitaLayout.createSequentialGroup()
                .addComponent(lblPreparadoSita, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pnlEntidad.setBackground(new java.awt.Color(255, 51, 51));

        lblPreparadoentidad.setText("Preparado para conciliar");

        chkEntidad.setBackground(new java.awt.Color(255, 51, 51));

        javax.swing.GroupLayout pnlEntidadLayout = new javax.swing.GroupLayout(pnlEntidad);
        pnlEntidad.setLayout(pnlEntidadLayout);
        pnlEntidadLayout.setHorizontalGroup(
            pnlEntidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlEntidadLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(chkEntidad, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblPreparadoentidad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlEntidadLayout.setVerticalGroup(
            pnlEntidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlEntidadLayout.createSequentialGroup()
                .addGroup(pnlEntidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(chkEntidad, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                    .addComponent(lblPreparadoentidad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        lblEntidadActual.setBackground(new java.awt.Color(255, 204, 204));
        lblEntidadActual.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblEntidadActual.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblEntidadActual.setText("En memoria: 0");
        lblEntidadActual.setOpaque(true);

        chkHistoriaconcilia.setBackground(new java.awt.Color(255, 204, 204));
        chkHistoriaconcilia.setText("Mantener Historico");

        btnStop.setText("Detener");
        btnStop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStopActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jButton4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnStop)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1))
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnCociliacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnSalidaConcilia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnGuardaSita, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jButton2)
                                    .addComponent(btnPend1)
                                    .addComponent(lblSitaActual, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(lblPendientesSita, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblPendientesEntidad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(btnSita, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(pnlSita, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(pnlEntidad, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(btnExtracto, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jButton3)
                            .addComponent(jButton6)
                            .addComponent(btnGuardaEntidad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblEntidadActual, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(chkHistoriaconcilia, javax.swing.GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE)
                                .addGap(1, 1, 1))))
                    .addComponent(lblProgreso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(19, 19, 19))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton4)
                    .addComponent(btnStop))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnGuardaEntidad, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblEntidadActual, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(btnExtracto, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(btnPend1, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(10, 10, 10)
                                            .addComponent(btnGuardaSita, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(btnSita, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblSitaActual, javax.swing.GroupLayout.DEFAULT_SIZE, 43, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlSita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(pnlEntidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPendientesEntidad, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblPendientesSita))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblProgreso)
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalidaConcilia)
                    .addComponent(chkHistoriaconcilia))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCociliacion, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        try {
            this.setInitValues();
            
        } catch (IOException ex) {
            RedebanLogger.escribeLogError(ex.getMessage());
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        RedebanLogger.escribeLogTrace(evt.getSource().toString());
        aux= new AuxArchivosWindow(this.rutaExtracto,this.nombreExtracto);
        aux.setVisible(true);
        aux.setLocationRelativeTo(this);
        Hilo h = new Hilo(1);
        h.start();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        RedebanLogger.escribeLogTrace(evt.getSource().toString());
        aux= new AuxArchivosWindow(this.rutaSita,this.nombreSita);
        aux.setVisible(true);
        aux.setLocationRelativeTo(this);
        Hilo h = new Hilo(0);
        h.start();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        RedebanLogger.escribeLogTrace(evt.getSource().toString());
        this.getPrincipal().toFront();
        this.setVisible(false);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnCociliacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCociliacionActionPerformed
        RedebanLogger.escribeLogTrace(evt.getSource().toString());
        btnCociliacion.setEnabled(false);
        procesarConciliacion();
        
    }//GEN-LAST:event_btnCociliacionActionPerformed

    private void btnExtractoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExtractoActionPerformed
        RedebanLogger.escribeLogTrace(evt.getSource().toString());
        procesarExtracto();
    }//GEN-LAST:event_btnExtractoActionPerformed

    private void btnSitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSitaActionPerformed
        RedebanLogger.escribeLogTrace(evt.getSource().toString());
        nuevaEntradaSita();
        
    }//GEN-LAST:event_btnSitaActionPerformed

    private void btnSalidaConciliaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalidaConciliaActionPerformed
        RedebanLogger.escribeLogTrace(evt.getSource().toString());
        aux= new AuxArchivosWindow(this.rutaConcilia,this.nombreConcilia);
        aux.setVisible(true);
        aux.setLocationRelativeTo(this);
        Hilo h = new Hilo(2);
        h.start();
        btnGuardaSita.setEnabled(true);
    }//GEN-LAST:event_btnSalidaConciliaActionPerformed

    private void btnPend1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPend1ActionPerformed
        RedebanLogger.escribeLogTrace(evt.getSource().toString());
        procesarPendientesSita();
    }//GEN-LAST:event_btnPend1ActionPerformed

    private void btnGuardaSitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardaSitaActionPerformed
        RedebanLogger.escribeLogTrace(evt.getSource().toString());
        guardarPendientesSita();
    }//GEN-LAST:event_btnGuardaSitaActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        RedebanLogger.escribeLogTrace(evt.getSource().toString());
        procesarPendientesEntidad();
    }//GEN-LAST:event_jButton6ActionPerformed

    private void btnGuardaEntidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardaEntidadActionPerformed
        RedebanLogger.escribeLogTrace(evt.getSource().toString());
        guardarPendientesEntidad();        
    }//GEN-LAST:event_btnGuardaEntidadActionPerformed

    private void btnStopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStopActionPerformed
        RedebanLogger.escribeLogTrace(evt.getSource().toString());
        detenerProcesos();        
    }//GEN-LAST:event_btnStopActionPerformed

    private void procesarExtracto(){
        String nombreSalida;
        String rutaDest;
        try {
            File inputFile = RBMUtils.seleccionArchivo("Seleccione extracto de la Entidad", "ZIP o CSV, Extracto entidad","zip","CSV");
            if (!nombreExtracto.isEmpty()) {
                nombreSalida = nombreExtracto;
            } else {
                nombreSalida = "SalidaProcesoEntidad";
            }
            if (!rutaExtracto.isEmpty()) {
                rutaDest = rutaExtracto;
            } else {
                rutaDest = ConfigRBM.getRutaProyecto();
            }
            procesoEntidad = new HiloProcesos(BANCO_PROCESO_NUEVO, inputFile.getAbsoluteFile(), lblProgreso, rutaDest, nombreSalida);
            procesoEntidad.setShow(true);
            lblEntidadActual.setBackground(Color.orange);
            procesoEntidad.start();
                
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Error Archivo Banco", ERROR_MESSAGE);
        }
    }
    
    private void nuevaEntradaSita() {
        
        try {
            File inputFile = RBMUtils.seleccionArchivo("Seleccione entrada de SITA","TXT, Archivo TXT", "txt");
            String rd = ConfigRBM.getRutaSita();
            String nf = ConfigRBM.getNombreSita();
            Set<String[]> data = new HashSet<>();
            if(procesoSita!=null)
                data.addAll(procesoSita.getDatosRta());
            
            procesoSita = new HiloProcesos(SITA_PROCESO_NUEVO, inputFile.getAbsoluteFile(), lblProgreso,rd,nf);
            
            procesoSita.setDatosRta(data);
            procesoSita.setShow(true);
            lblSitaActual.setBackground(Color.orange);
            procesoSita.start();
        
            
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), ETIQUETA_ERROR_SITA, ERROR_MESSAGE);
            RedebanLogger.escribeLogError("Error en Sita "+ex.getMessage());
        }

    }
    
    private void guardarPendientesSita(){
        
        try {
            
            String ruta= ConfigRBM.getRutaPendientesSita();
            String name = ConfigRBM.getNombrePendientesSita();
            HiloProcesos pSita = new HiloProcesos(SITA_PROCESO_GUARDAR_TEMPORAL, null, lblProgreso, ruta, name);
            lblSitaActual.setBackground(Color.orange);
            pSita.setDatosRta(procesoSita.getDatosRta());
            pSita.start();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), ETIQUETA_ERROR_SITA, ERROR_MESSAGE);
        }

    }
    
        private void guardarPendientesEntidad(){
        
        try {
            
            String ruta= ConfigRBM.getRutaPendientesEntidad();
            String name = ConfigRBM.getNombrePendientesEntidad();
            HiloProcesos pEntidad = new HiloProcesos(BANCO_PROCESO_GUARDAR_TEMPORAL, null, lblProgreso, ruta, name);
            pEntidad.setDatosRta(procesoEntidad.getDatosRta());
            pEntidad.start();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), ETIQUETA_ERROR_SITA, ERROR_MESSAGE);
        }

    }

    
        
    
        private void procesarPendientesSita() {
        
        try {
            File inputFile = RBMUtils.seleccionArchivo("Seleccione Pendientes por conciliar de SITA","xlsx, Archivo xlsx", "xlsx");
            if (inputFile != null) {

                ArchivoSita.setSitaData(null);
                ArchivoSita.setNuevoArchivo(null);
                procesoSita = new HiloProcesos(SITA_PROCESO_PENDIENTES, inputFile.getAbsoluteFile(), lblProgreso);
                procesoSita.setShow(true);
                lblSitaActual.setBackground(Color.orange);
                procesoSita.start();
            }
            
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), ETIQUETA_ERROR_SITA, ERROR_MESSAGE);
        }

    }
        
        private void procesarPendientesEntidad() {
        
        try {
            File inputFile = RBMUtils.seleccionArchivo("Seleccione Pendientes por conciliar de Entidad", "xlsx, Archivo xlsx", "xlsx");
            if (inputFile != null) {

                ExtractoBanco.setExtractoData(null);
                ExtractoBanco.setNuevoArchivo(null);

                procesoEntidad = new HiloProcesos(BANCO_PROCESO_PENDIENTES, inputFile.getAbsoluteFile(), lblProgreso);
                procesoEntidad.setShow(true);
                lblEntidadActual.setBackground(Color.orange);
                procesoEntidad.start();
            }
            
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), ETIQUETA_ERROR_SITA, ERROR_MESSAGE);
        }

    }
        
    private void procesarConciliacion() {

        String nombreSalida;
        String rutaDest;
        if (!procesoSita.isEstado()) {
            return;
        }
        if (!procesoEntidad.isEstado()) {
            return;
        }

        if (!nombreConcilia.isEmpty()) {
            nombreSalida = nombreConcilia;
        } else {
            nombreSalida = "SalidaProcesoConciliacion";
        }

        if (!rutaConcilia.isEmpty()) {
            rutaDest = rutaConcilia;
        } else {
            rutaDest = "SalidaProcesoConciliacion";
        }
        procConcilia = new HiloConcilia(lblProgreso, rutaDest, nombreSalida);
        procConcilia.setDatosEntidad(procesoEntidad.getDatosRta());
        procConcilia.setDatosSita(procesoSita.getDatosRta());
        procConcilia.start();

    }

        private class Hilo extends Thread {

            int tipoProceso;

            private Hilo(int tipo) {
                tipoProceso = tipo;
            }
            boolean run = true;

            public void terminate() {
                run = false;
            }

            @Override
            public void run() {
                while (run) {

                    if (aux.isConfirmacion()) {
                        switch (tipoProceso) {
                            case 0:
                                rutaSita = aux.getRutaArchivo();
                                nombreSita = aux.getNombreArchivo();
                                break;
                            case 1:
                                rutaExtracto = aux.getRutaArchivo();
                                nombreExtracto = aux.getNombreArchivo();
                                break;
                            default:
                                rutaConcilia = aux.getRutaArchivo();
                                nombreConcilia = aux.getNombreArchivo();
                                break;
                        }
                        aux.setConfirmacion(false);
                        aux.setVisible(false);
                        terminate();
                    }
                }
            }
    }
        
    public void initWindow() throws IOException{
        try {
            String iconoString = ConfigRBM.getRutaProyecto() + "/recursos/imagenes/icono.png";
            RedebanLogger.escribeLogInfo("ruta icono: "+iconoString);
            File pathToFile = new File(iconoString);
            Image image = ImageIO.read(pathToFile);
            setIconImage(image);
        } catch (IOException ex) {
            RedebanLogger.escribeLogError(ex.getMessage());
        }
        setTitle(PROYECT_NAME.replaceAll(".jar", ""));
        procesoSita = null;
        procesoEntidad = null;
        procesoConciliacion = null;
        procConcilia = null;
        lblProgreso.setText("Estado: por iniciar");
        this.nombreSita = ConfigRBM.getNombreSita();
        this.nombreExtracto = ConfigRBM.getNombreEntidad();
        this.rutaSita = ConfigRBM.getRutaSita();
        this.rutaExtracto = ConfigRBM.getRutaEntidad();
        this.nombreConcilia=ConfigRBM.getNombreConciliacion();
        this.rutaConcilia=ConfigRBM.getRutaConcilia();
        this.chkEntidad.setBackground(Color.red);
        this.chkEntidad.setSelected(false);
        lblPreparadoentidad.setBackground(Color.red);
        pnlEntidad.setBackground(Color.red);
        
        this.chkSita.setBackground(Color.red);
        this.chkSita.setSelected(false);
        lblPreparadoSita.setBackground(Color.red);
        pnlSita.setBackground(Color.red);
        
        btnGuardaSita.setEnabled(false);
        btnGuardaEntidad.setEnabled(false);
        ArchivoSita.setNuevoArchivo(null);
        ExtractoBanco.setNuevoArchivo(null);
        ArchivoSita.setSitaData(null);
        ExtractoBanco.setExtractoData(null);
        
        lblSitaActual.setText(ETIQUETA_MEMORIA_CERO);
        lblEntidadActual.setText(ETIQUETA_MEMORIA_CERO);
        lblSitaActual.setBackground(Color.gray);
        lblEntidadActual.setBackground(Color.gray);
        lblPendientesSita.setText(ETIQUETA_SIN_PENDIENTES);
        lblPendientesEntidad.setText(ETIQUETA_SIN_PENDIENTES);
        cargarPendientes(false);

        

        
    }

    private class HiloConcilia extends Thread {

        /**
         * @return the datosSita
         */
        public Set<String[]> getDatosSita() {
            return datosSita;
        }

        /**
         * @param datosSita the datosSita to set
         */
        public void setDatosSita(Set<String[]> datosSita) {
            this.datosSita = datosSita;
        }

        /**
         * @return the datosEntidad
         */
        public Set<String[]> getDatosEntidad() {
            return datosEntidad;
        }

        /**
         * @param datosEntidad the datosEntidad to set
         */
        public void setDatosEntidad(Set<String[]> datosEntidad) {
            this.datosEntidad = datosEntidad;
        }

        JLabel lblProgreso;
        private String rutaDestino;
        private String nameOut;

        private Set<String[]> datosSita;
        private Set<String[]> datosEntidad;


        private HiloConcilia() {
        }

        HiloConcilia(JLabel lbl, String rd, String n) {

            this.lblProgreso = lbl;
            this.rutaDestino = rd;
            this.nameOut = n;

        }

        @Override
        public void run() {

            try {
                if (datosSita != null && datosEntidad != null) {
                    procesoConciliacion = new Conciliacion();
                    procesoConciliacion.init(lblProgreso);
                    procesoConciliacion.setSalida(nameOut, rutaDestino);
                    procesoConciliacion.setHistorico(chkHistoriaconcilia.isSelected());
                    
                    
                    //TODO cambiar a fucion personalizada para salida especial
                    for(int i=0;i<ExtractoInfo.COLUMNA_FINAL;i++){
                        procesoConciliacion.addColsSalida(1, ExtractoInfo.COLUMNA_INICIAL + i);
                    }
                    
                    for(int i=0;i<SitaInfo.COLUMNA_FINAL;i++){
                        procesoConciliacion.addColsSalida(0, SitaInfo.COLUMNA_INICIAL + i);
                    }
                    
                    procesoConciliacion.setHeaderSita(getHeaderSita());
                    procesoConciliacion.setHeaderEntidad(getHeaderEntidad());
                    //TODO cambiar a fucion personalizada para salida especial
                    procesoConciliacion.setHeaderConcilia(getHeaderEntidad()+";"+getHeaderSita());
                    
                    procesoConciliacion.ejecutarProceso(datosSita, datosEntidad);
                    initWindow();
                    
                } else {
                    RedebanLogger.escribeLogError("Selecciones entradas de conciliacion!!!");
                    mostrarMensaje("Error", "Selecciones entradas de conciliacion!!!", JOptionPane.ERROR_MESSAGE);
                }

            } catch (IOException ex) {
                RedebanLogger.escribeLogError(ex.getMessage());
                mostrarMensaje("Error", "Error en proceso de Conciliacion: \n" + ex.getMessage(), JOptionPane.ERROR_MESSAGE);
            }
        }

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCociliacion;
    private javax.swing.JButton btnExtracto;
    private javax.swing.JButton btnGuardaEntidad;
    private javax.swing.JButton btnGuardaSita;
    private javax.swing.JButton btnPend1;
    private javax.swing.JButton btnSalidaConcilia;
    private javax.swing.JButton btnSita;
    private javax.swing.JButton btnStop;
    private javax.swing.JCheckBox chkEntidad;
    private javax.swing.JCheckBox chkHistoriaconcilia;
    private javax.swing.JCheckBox chkSita;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblEntidadActual;
    private javax.swing.JLabel lblPendientesEntidad;
    private javax.swing.JLabel lblPendientesSita;
    private javax.swing.JLabel lblPreparadoSita;
    private javax.swing.JLabel lblPreparadoentidad;
    private javax.swing.JLabel lblProgreso;
    private javax.swing.JLabel lblSitaActual;
    private javax.swing.JPanel pnlEntidad;
    private javax.swing.JPanel pnlSita;
    // End of variables declaration//GEN-END:variables
}
