/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.redeban;

import com.redeban.tipos.InfoBase;
import com.redeban.tipos.LlaveData;
import com.redeban.utils.ConfigRBM;
import com.redeban.utils.RedebanLogger;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.JLabel;

/**
 *
 * @author rm
 */
public class InfoComercio extends InfoBase{

    public void filtrarEntreada() {

    }


    public int filtrarEntrada(File pendientes, String hoja, int offset, JLabel lbl) throws IOException {


        Set<List<String>> datos = LeerFicherosExcel.leerArchivoExcel(pendientes, hoja, offset, 0, true, lbl);
        return filtrarDetalle(datos, offset);

    }
    
    
    public int filtrarDetalle(Set<List<String>> datos, int offset) throws IOException {

        int count = 0;
        String line = "";
        Iterator<List<String>> it = datos.iterator();
        while (it.hasNext()) {
            StringBuilder tmp = new StringBuilder();

            count++;
            if (count <= offset) {
                continue;
            }
            List<String> lista = it.next();
            for (int i = 0; i < lista.size(); i++) {
                tmp.append(lista.get(i) + this.getCharSeparator());
            }
            line = tmp.toString();
            
            if(!validarCondicionesComercios(line))

                throw new IOException("Error en registro de comercios linea:"+count+"\n"+line);

            LlaveData llaveBase = obtieneLlaveBase(line);
            if (1 == (agegaRegistroBase(llaveBase, line))) {
                RedebanLogger.escribeLogInfo("Registro pendiente agregado: " + line);
            }
        }
        return (count - offset);

    }

    private boolean validarVacios(String[] data) {
        boolean res = true;

        try {
            String vacios = ConfigRBM.getComerciosColumnaValidaVacios();
            
            

            for (String col : vacios.split(",")) {
                
                if(Integer.parseInt(col)>data.length){
                    res = false;
                    break;
                }

                if (data[Integer.parseInt(col)].length() == 0) {
                    res = false;
                    break;
                }

            }
        } catch (IOException ex) {
            RedebanLogger.escribeLogError("Parametro valida vacios no encontrado");
        }
        return res;

    }

    private boolean validarEntradaProducto(String[] data) {
        boolean res = true;

        try {
            int colmunaProducto = Integer.parseInt(ConfigRBM.getComerciosColumnaProducto());

            String columnasValidacion = ConfigRBM.getComerciosColumnasProductoContenido();

            if (colmunaProducto > data.length) {
                res = false;
            } else {

                String producto = data[colmunaProducto];
                String validador = ConfigRBM.getComerciosColumnaProductoNoVacio();

                if (producto.equalsIgnoreCase(validador)) {
                    for (String col : columnasValidacion.split(",")) {
                        int columna = Integer.parseInt(col);
                        if (columna > data.length) {
                            res = false;
                            break;
                        }
                        if (data[columna].length() == 0) {
                            res = false;
                            break;
                        }
                    }
                }

            }

        } catch (IOException ex) {
            RedebanLogger.escribeLogError("Parametro valida vacios no encontrado");
        }
        return res;
    }

    private boolean validarCondicionesComercios(String registro) {
        boolean res = true;

        String[] datos = registro.split("" + this.getCharSeparator());
        if (datos.length > 0) {
            res = validarVacios(datos);

            if (res) {
                res = validarEntradaProducto(datos);
            }

        }

        return res;
    }

}
