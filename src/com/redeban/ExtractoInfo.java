/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.redeban;

import com.redeban.tipos.InfoBase;
import com.redeban.tipos.LlaveData;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author jmtoro
 */
public class ExtractoInfo extends InfoBase{

    public static final int COLUMNA_INICIAL = 0;
    public static final int COLUMNA_CAMPO_TIPO_TRANSACCION = 0;
    public static final int COLUMNA_CAMPO_CU = 1;
    
    public static final int COLUMNA_CAMPO_FRANQUICIA = 2;
    
    public static final int COLUMNA_CAMPO_CUENTA = 3;
    
    public static final int COLUMNA_CAMPO_FECHA_VALE = 4;
    public static final int COLUMNA_CAMPO_FECHA_PROCESO = 5;
    public static final int COLUMNA_CAMPO_FECHA_ABONO = 6;
    
    public static final int COLUMNA_CAMPO_ULTIMOSCUATRO = 7;
    
    public static final int COLUMNA_CAMPO_HORA_TXN = 8;
    public static final int COLUMNA_CAMPO_RECIBO=9;
    public static final int COLUMNA_CAMPO_APROBACION=10;
    public static final int COLUMNA_CAMPO_TERMINAL = 11;
    public static final int COLUMNA_CAMPO_TOTAL2 = 12;
    public static final int COLUMNA_CAMPO_IVA = 13;
    public static final int COLUMNA_CAMPO_PROPINA = 14;
    public static final int COLUMNA_CAMPO_INC = 15;
    
    public static final int COLUMNA_CAMPO_TOTAL = 16;
    
    public static final int COLUMNA_CAMPO_COMISION = 17;
    public static final int COLUMNA_CAMPO_RETE_IVA = 18;
    public static final int COLUMNA_CAMPO_RETE_ICA = 19;
    public static final int COLUMNA_CAMPO_RETE_FUENTE = 20;
    public static final int COLUMNA_CAMPO_ABONO = 21;
    public static final int COLUMNA_CAMPO_TIPO_TARJETA = 22;
    
    public static final int COLUMNA_FINAL = 23;
    
    public static final int TIPO_CREDITO =0;
    public static final int TIPO_DEBITO =1;
    
    private static final int MAX_ESTADOS=2;


    
    public ExtractoInfo(String cabecera, char ch) {
        
        
        setCharSeparator(ch);
        setListaCampos(cabecera.split( String.valueOf(ch)));
        setRegistrosBase(new ConcurrentHashMap<>());
        setMaxEstados(MAX_ESTADOS);
        
    }   
    
    public ExtractoInfo(String cabecera, int size,char ch) {
        
        
        setCharSeparator(ch);
        setListaCampos(cabecera.split( String.valueOf(ch)));
        setRegistrosBase(new ConcurrentHashMap<>(size));
        setMaxEstados(MAX_ESTADOS);

    }
    
    public boolean filtarRegistros(int idColumna, String[] input, String valor) {

        return input[idColumna].equals(valor);
    }
    
    @Override
    public int agegaRegistroBase(LlaveData llave, String valor) {

        String [] dataEntrada = valor.split( String.valueOf(getCharSeparator()));
        String tipoTxn = dataEntrada[this.getIndexPosTipoTXN()];
        int tipo=0;
        int numericoTipoTxn= Integer.parseInt(tipoTxn);
        if(numericoTipoTxn == 6){
            tipo = TIPO_CREDITO;
        } else if (numericoTipoTxn==26) {
            tipo = TIPO_DEBITO;
        }
        else{
            return 2;
        }
        
        return agegaRegistroBaseFinal(llave,valor,tipo);
    }
    
}

