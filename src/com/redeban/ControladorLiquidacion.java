package com.redeban;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.redeban.tipos.InfoBase;
import com.redeban.tipos.InfoRegistro;
import com.redeban.tipos.LlaveData;
import com.redeban.utils.ConfigRBM;
import static com.redeban.utils.DEFINITIONS.ESTADO_NO_LIQUIDADO;
import static com.redeban.utils.DEFINITIONS.HOJA_UNO;
import com.redeban.utils.RedebanLogger;
import com.redeban.windows.MainWondow;
import java.awt.Color;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author jmtoro
 */
public class ControladorLiquidacion extends Thread {
    
    private static int COLUMNA_PORCENTAJE_COMISION=42;
    private static int COLUMNA_COMISION_FIJA=43;
    private static int COLUMNA_COMISION_FIJA_IVA=44;


    /**
     * @return the historico
     */
    public boolean isHistorico() {
        return historico;
    }

    /**
     * @param historico the historico to set
     */
    public void setHistorico(boolean historico) {
        this.historico = historico;
    }

    /**
     * @return the separator
     */
    public char getSeparator() {
        return separator;
    }

    /**
     * @param separator the separator to set
     */
    public void setSeparator(char separator) {
        this.separator = separator;
    }

    /**
     * @return the infoLiquidacion
     */
    public InfoBase getInfoLiquidacion() {
        return infoLiquidacion;
    }

    /**
     * @param infoLiquidacion the infoLiquidacion to set
     */
    public void setInfoLiquidacion(InfoBase infoLiquidacion) {
        this.infoLiquidacion = infoLiquidacion;
    }

    /**
     * @return the columnaProducto
     */
    public int getColumnaProducto() {
        return columnaProducto;
    }

    /**
     * @param columnaProducto the columnaProducto to set
     */
    public void setColumnaProducto(int columnaProducto) {
        this.columnaProducto = columnaProducto;
    }
    private static final int ID_COLUMNA_FECHA_FILTRO=24;
    private static final int ID_COLUMNA_PRODUCTO_FILTRO=9;
    private static final String PRODUCTO_SIN_FILTRO="Todos";
    
    private static final int ID_COLUMNA_TERMINAL=31;
    private static final int ID_COLUMNA_CODIGO_UNICO=30;
    

    /**
     * @return the productos
     */
    public String getProductos() {
        return productos;
    }

    /**
     * @param productos the productos to set
     */
    public void setProductos(String productos) {
        this.productos = productos;
    }

    /**
     * @return the fechaIni
     */
    public Date getFechaIni() {
        return fechaIni;
    }

    /**
     * @param fechaIni the fechaIni to set
     */
    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }

    /**
     * @return the fechaFin
     */
    public Date getFechaFin() {
        return fechaFin;
    }

    /**
     * @param fechaFin the fechaFin to set
     */
    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    /**
     * @return the infoComercios
     */
    public InfoBase getInfoComercios() {
        return infoComercios;
    }

    /**
     * @param infoComercios the infoComercios to set
     */
    public void setInfoComercios(InfoBase infoComercios) {
        this.infoComercios = infoComercios;
    }

    /**
     * @return the infoConciliados
     */
    public InfoBase getInfoConciliados() {
        return infoConciliados;
    }

    /**
     * @param infoConciliados the infoConciliados to set
     */
    public void setInfoConciliados(InfoBase infoConciliados) {
        this.infoConciliados = infoConciliados;
    }

    
    private String rutaSalida;
    private String nombreSalida;
    
    private InfoBase infoConciliados;
    private InfoBase infoComercios;
    
    
    
    private InfoBase infoLiquidacion;
    
    
    private Date fechaIni;
    private Date fechaFin;
    private String productos;
    private int columnaFecha;
    private int columnaProducto;
    private ArrayList<int []> relacionLlaves;
    private char separator;
    private boolean historico;
    
    int contador=0;
    int previos = 0;
    int omitidos = 0;
    int omitidosFecha = 0;
    int omitidosInfo = 0;
    int omitidosProducto = 0;
    int liquidados = 0;
    int liquidadosPSE = 0;
    
    

    JLabel lblProgreso;

     public ControladorLiquidacion(JLabel lbl) {
        this.lblProgreso = lbl;
        relacionLlaves = new ArrayList<>();
    }
     
    public ControladorLiquidacion( String r, String  nf, JLabel lbl) {
        this.rutaSalida = r;
        this.nombreSalida = nf;
        this.lblProgreso = lbl;
        relacionLlaves = new ArrayList<>();
    }
    
    public void setColumnaFecha(int idCol){
        columnaFecha =idCol;
    }
    
    public boolean filtroFecha(Map.Entry<LlaveData, InfoRegistro> entry){
        boolean res = false;
        String fechaRegistro = entry.getValue().getRegistro()[getColumnaFecha()];
        SimpleDateFormat sdfrmt = new SimpleDateFormat("yyyy/MM/dd");
        sdfrmt.setLenient(false);

        try {
            Date dateActual = sdfrmt.parse(fechaRegistro);
            if (dateActual.compareTo(fechaIni) >= 0 && dateActual.compareTo(fechaFin) <= 0 ) {
                RedebanLogger.escribeLogInfo("Date act occurs betwen 2 dates");
                res = true;
            }
            else{
                omitidosFecha++;
            }
        } catch (ParseException ex) {
            RedebanLogger.escribeLogError(ex.getLocalizedMessage());
        }

        return res;
    }
    
    public boolean filtroLiquidado(Map.Entry<LlaveData, InfoRegistro> entry){
        boolean res = true;
        
        String estado = entry.getValue().getRegistro()[entry.getValue().getRegistro().length-1];
        
        if(!estado.equals(ESTADO_NO_LIQUIDADO)){
            previos++;
            RedebanLogger.escribeLogError("Registro ya Liquidado..." + Arrays.toString(entry.getKey().getCampos()));
            res =false;
        }
      
        return res;
    }
    
    
    public boolean filtroProducto(Map.Entry<LlaveData, InfoRegistro> entry) {
        boolean res = true;
      
        String[] dataComercio = getInfoComercioTransaccion(entry);
        if (dataComercio != null) {
            String productoActual = dataComercio[getColumnaProducto()].toUpperCase();
            if (!getProductos().equalsIgnoreCase(productoActual) && !getProductos().equalsIgnoreCase(PRODUCTO_SIN_FILTRO.toUpperCase())) {
                omitidosProducto++;
                res = false;
            }

        } else {
            omitidosInfo++;
            RedebanLogger.escribeLogError("Registro no encontrado..." + Arrays.toString(entry.getKey().getCampos()));
            res = false;
        }
        

        
        return res;
    }
    
    public boolean filtroCondiciones(Map.Entry<LlaveData, InfoRegistro> entry) {
        
        boolean res = filtroLiquidado(entry);
        if (res) {
            res = filtroFecha(entry);
        }
        if (res) {
            res = filtroProducto(entry);
        }
        return res;
    }
    
    public String prepararDataLiquidacion(String[] infoTransaccion,char separator,String [] infoComercio){
        
        String res = Arrays.toString(infoTransaccion).replaceAll("[^a-zA-Z0-9;, @./]", " ");
        
        res += separator+Arrays.toString(infoComercio).replaceAll("[^a-zA-Z0-9;, @./]", " ");
        
        return res;
    }
    
    String obtenerCalculos(String[] infoTransaccion,String [] infoComercio){
        
        String res=null;
        try {
            
            String[] columnasCalc = new String[6];
            double valorTxn = Double.parseDouble(infoTransaccion[12]);
            double porcentajeComision = Double.parseDouble(infoComercio[COLUMNA_PORCENTAJE_COMISION]);

            double ivaAdicionalComision = 0;

            int col0 = (int) Math.round(valorTxn * porcentajeComision);

            columnasCalc[0] = String.valueOf(col0);

            String infoAux = infoComercio[COLUMNA_COMISION_FIJA];
            if (!infoAux.isEmpty()) {
                columnasCalc[1] = infoAux;

            } else {
                columnasCalc[1] = "0";
            }

            if (!infoComercio[COLUMNA_COMISION_FIJA_IVA].isEmpty()) {
                ivaAdicionalComision = Double.parseDouble(infoComercio[COLUMNA_COMISION_FIJA_IVA]);
            }

            double valorAdicionalTxn = Double.parseDouble(columnasCalc[1]);

            int col1 = (int) Math.round(valorAdicionalTxn * ivaAdicionalComision);
            columnasCalc[2] = String.valueOf(col1);

            double valorTotalComision = (valorTxn * porcentajeComision) + (valorAdicionalTxn) + (valorAdicionalTxn * ivaAdicionalComision);
            int col2 = (int) Math.round(valorTotalComision);
            columnasCalc[3] = String.valueOf(col2);

            double valorComisionBanco = Double.parseDouble(infoTransaccion[17]);
            int col3 = (int) Math.round(Double.parseDouble(columnasCalc[3]) - valorComisionBanco);
            columnasCalc[4] = String.valueOf(col3);

            double valorTotal = Math.round(Double.parseDouble(infoTransaccion[16]));
            double valorReteIva = Math.round(Double.parseDouble(infoTransaccion[18]));
            double valorReteIca = Math.round(Double.parseDouble(infoTransaccion[19]));
            double valorReteFuente = Math.round(Double.parseDouble(infoTransaccion[20]));
            valorTotalComision = Math.round(valorTotalComision);

            RedebanLogger.escribeLogError("valores " + valorReteIva + " " + valorReteIca + " " + valorReteFuente + " + " + valorTotalComision);

            RedebanLogger.escribeLogError("valoresT0 " + valorTotal);
            double descuentos = valorReteIva + valorReteIca + valorReteFuente + valorTotalComision;
            double valorAbono = valorTotal - descuentos;

            int col4 = (int) Math.round(valorAbono);
            columnasCalc[5] = String.valueOf(col4);

            res = Arrays.toString(columnasCalc).replaceAll("[\\]]", ",");
            res = res.replaceAll("[^a-zA-Z0-9;, @./]-", "").substring(1, res.length() - 1);

        } catch (NumberFormatException ex) {
            RedebanLogger.escribeLogError(ex.getLocalizedMessage());
        }


        return res;
    }
    
    public void liquidarRegistro(Map.Entry<LlaveData, InfoRegistro> entry){
        String[] dataComercio = getInfoComercioTransaccion(entry);
        
        
        if(dataComercio==null)
        {
            RedebanLogger.escribeLogInfo("null");
        }

        String datos = Arrays.toString(entry.getValue().getRegistro()).replaceAll("[^a-zA-Z0-9;, @./]", " ");
        
        datos += separator + obtenerCalculos(entry.getValue().getRegistro(), dataComercio);

        datos += separator + Arrays.toString(dataComercio).replaceAll("[^a-zA-Z0-9;, @./]", " ");

        

        getInfoLiquidacion().agegaRegistroBase(entry.getKey(), datos);


        
    }
    
    public String[] getInfoComercioTransaccion(Map.Entry<LlaveData, InfoRegistro> entry) {
        
        String[] llave = new String[relacionLlaves.size()];
        String[] response = null;
        int i = 0;
        for (int[] columna : relacionLlaves) {
            
            llave[i++] = entry.getValue().getRegistro()[columna[1]];
        }

        LlaveData llaveBusqueda = new LlaveData(llave);
        
        InfoRegistro registro = getInfoComercios().getRegistrosBase().get(llaveBusqueda);
        if (registro != null) {
            response = registro.getRegistro();
        }
        
        return response;
    }
   
    public void procesarConciliacion(InfoBase registros, int tipo) {

        int pasos = registros.getRegistrosBase().size();

        for (Map.Entry<LlaveData, InfoRegistro> entry : registros.getRegistrosBase().entrySet()) {
            contador++;
            float porcentaje = 0;
            if (pasos != 0) {
                porcentaje = (float) contador * 100 / (float) pasos;
            }

            lblProgreso.setText("Progreso: " + porcentaje + "%");

            if (tipo == 0) {
                if (!filtroCondiciones(entry)) {
                    RedebanLogger.escribeLogError("Registro para liquidacion omitido: " + Arrays.toString(entry.getKey().getCampos()));
                    omitidos++;
                    continue;
                }
                liquidados++;
                LlaveData llave = entry.getKey();
                entry.getValue().getRegistro()[entry.getValue().getRegistro().length - 1] = "Liquidado_" + getActualDate("yyyy_MM_dd_HH_mm");

                infoConciliados.getRegistrosBase().put(llave, entry.getValue());
                liquidarRegistro(entry);
            }

        }
    }

    private void addRelacionLlaves(int columnaComercio,int columnaConciliados){
        int[] llaves = new int[2];
        llaves[0]=columnaComercio;
        llaves[1]=columnaConciliados;
        relacionLlaves.add(llaves);
    }
    
    /**
     *
     * @param patron
     * @return
     */
    public String getActualDate(String patron) {
        String res = null;
        String pattern = patron;

        DateFormat df = new SimpleDateFormat(pattern);

        Date today = Calendar.getInstance().getTime();

        res = df.format(today);
        return res;
    }
    
    public void generarReporteLiquidacion(String detalleArchivo) throws IOException {
        Set<String[]> response = null;
        try {

            int lenFile = getInfoLiquidacion().sizeData();

            response = new HashSet<>();

            ExcelSalida archivoLiquidacion = new ExcelSalida(lenFile, nombreSalida + detalleArchivo, rutaSalida, HOJA_UNO, lblProgreso);

            float steps = 100 / (float) lenFile;
            int count = 0;
            for (Map.Entry<LlaveData, InfoRegistro> entry : getInfoLiquidacion().getRegistrosBase().entrySet()) {
                count++;
                lblProgreso.setText("Generando Reporte: " + steps * count);
                RedebanLogger.escribeLogInfo("Generando Reporte: " + steps * count);

                archivoLiquidacion.addMatrizData((Object[])entry.getValue().getRegistro());
                response.add(entry.getValue().getRegistro());

            }

            

            String[] columnasAdd = new String[6];
            columnasAdd[0]="Comision a cobrar".toUpperCase();
            columnasAdd[1]="Costo Adicional".toUpperCase();
            columnasAdd[2]="IVA Costo adicional".toUpperCase();
            columnasAdd[3]="Valor total de comision".toUpperCase();
            columnasAdd[4]="Utilidad liquidacion".toUpperCase();
            columnasAdd[5]="Valor a pagar a comercio".toUpperCase();
            archivoLiquidacion.crearExcelsalida(isHistorico(), getInfoConciliados().getListaCampos(),columnasAdd,getInfoComercios().getListaCampos());

        } catch (IOException ex) {
            throw new IOException(ex.getMessage());
        }
        
    }
    
    public void actualizarReporteConciliacion(){
        Set<String[]> response =new HashSet<>();
        try {
            
            String rutaLiquidacion = ConfigRBM.getRutaEntradaLiquidacion();
            String name = ConfigRBM.getNombreEntradaLiquidacion();

            int lenFile = getInfoConciliados().sizeData();

            ExcelSalida archivoConciliacion = new ExcelSalida(lenFile, name , rutaLiquidacion, HOJA_UNO, lblProgreso);

            float steps = 100 / (float) lenFile;
            int count = 0;
            for (Map.Entry<LlaveData, InfoRegistro> entry : getInfoConciliados().getRegistrosBase().entrySet()) {
                count++;
                lblProgreso.setText("Actualizando Conciliacion: " + steps * count);
                RedebanLogger.escribeLogInfo("Actualizando Conciliacion: " + steps * count);

                archivoConciliacion.addMatrizData((Object[])entry.getValue().getRegistro());
                response.add(entry.getValue().getRegistro());

            }

            String[] header = new String[getInfoConciliados().getListaCampos().length];
            System.arraycopy(getInfoConciliados().getListaCampos(), 0, header, 0, getInfoConciliados().getListaCampos().length);
            getInfoConciliados().setListaCampos(header);

            archivoConciliacion.crearExcelsalida(false, (Object[])getInfoConciliados().getListaCampos());

        } catch (IOException ex) {
            RedebanLogger.escribeLogError(ex.getLocalizedMessage());
            
        }
        
        
    }

    @Override
    public void run() {
        contador = 0;
        omitidos = 0;
        omitidosFecha = 0;
        omitidosProducto = 0;
        liquidados = 0;
        liquidadosPSE = 0;
        previos=0;
        omitidosInfo=0;
    
        RedebanLogger.escribeLogError("paso 10");
        try {
            if(getInfoConciliados()==null || getInfoComercios() == null || getFechaIni() == null || getFechaFin() == null) {
                MainWondow.mostrarMensaje("ERROR DE PROCESO", "ERROR EN PARAMETROS DE PROCESO, POR FAVOR VERIFIQUE!!!", JOptionPane.ERROR_MESSAGE);
            }
            lblProgreso.setText("Progreso: 1%");
            
            setColumnaFecha(ID_COLUMNA_FECHA_FILTRO);
            setColumnaProducto(ID_COLUMNA_PRODUCTO_FILTRO);
            addRelacionLlaves(0,ID_COLUMNA_TERMINAL);
            addRelacionLlaves(14,ID_COLUMNA_CODIGO_UNICO);
            setSeparator(',');
            
            infoLiquidacion = new InfoBase();
            infoLiquidacion.setCharSeparator(',');
            infoLiquidacion.setMaxEstados(1);
            infoLiquidacion.setRegistrosBase(new ConcurrentHashMap<>());
            
            
            procesarConciliacion(getInfoConciliados(),0);
            
            
            
            generarReporteLiquidacion(getActualDate("yyMMddHHmm"));
            generarReporteLiquidacion("_historico");
            
            actualizarReporteConciliacion();
            
            MainWondow.mostrarMensaje("RESULTADO DEL PROCESO", "Resultado Liquidacion: "
                + "\n entrada Actual                       : " + contador
                + "\n registros liquidados                 : " + liquidados
                + "\n registros omitidos                   : " + omitidos
                + "\n registros liquidados previamente     : " + previos
                + "\n registros omitidos por Info Comercio : " + omitidosInfo
                + "\n registros omitidos por Fecha         : " + omitidosFecha
                + "\n registros omitidos por Producto      : " + omitidosProducto
                + "\n\nPara consultar detalle de Liquidacion vaya a:"
                + rutaSalida, 1);
            
            
            lblProgreso.setBackground(Color.green);
            lblProgreso.setText("Registros liquidados: "+liquidados);

        } catch (IOException ex) {
            Logger.getLogger(ControladorLiquidacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the columnaFecha
     */
    public int getColumnaFecha() {
        return columnaFecha;
    }

    
}
