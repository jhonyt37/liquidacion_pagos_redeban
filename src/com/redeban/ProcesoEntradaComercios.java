package com.redeban;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.redeban.tipos.InfoBase;
import com.redeban.utils.ConfigRBM;
import com.redeban.utils.RBMUtils;
import com.redeban.utils.RedebanLogger;
import com.redeban.windows.MainWondow;


import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import javax.swing.JLabel;

/**
 *
 * @author jmtoro
 */
public class ProcesoEntradaComercios extends Thread {

    /**
     * @return the infoComercios
     */
    public InfoComercio getInfoComercios() {
        return infoComercios;
    }

    /**
     * @param infoComercios the infoComercios to set
     */
    public void setInfoComercios(InfoComercio infoComercios) {
        this.infoComercios = infoComercios;
    }
    
    private File entradaFile;
    
    private InfoComercio infoComercios;
    
    private int [] tipoLiquidacion;

    JLabel lblProgreso;

    public ProcesoEntradaComercios( File f, JLabel lbl) {
        entradaFile = f;
        lblProgreso = lbl;
    }
    
    public void setLlaveComercios(int ... llaves){
        setTipoLiquidacion(llaves);
    }
    
    
    @Override
    public void run() {

        boolean res=true;

        
        setInfoComercios(new InfoComercio());
        
        getInfoComercios().setMaxEstados(1);
        getInfoComercios().setCharSeparator(',');
        if (this.getTipoLiquidacion() != null) {
            getInfoComercios().defineCamposLlaveBase(this.getTipoLiquidacion());
        } else {
            getInfoComercios().defineCamposLlaveBase(27, 28);
        }
        getInfoComercios().setRegistrosBase(new ConcurrentHashMap<>());
        
        try {
            
            String hoja = ConfigRBM.getNombreHojaInfoComercios();
            
            String campos = RBMUtils.obtenerHeader(entradaFile, hoja);
            
            campos = campos.replaceAll("[^a-zA-Z0-9;, @./]", "").toUpperCase();
            getInfoComercios().setListaCampos(campos.split(","));
            
            getInfoComercios().filtrarEntrada(entradaFile,hoja, 1,lblProgreso);
            
        } catch (IOException ex) {
            res=false;
            RedebanLogger.escribeLogError(ex.getMessage());
            MainWondow.mostrarMensaje("Error en carga",ex.getMessage(),1);
            
            
        }
        if(res){
        lblProgreso.setBackground(Color.green);
        lblProgreso.setText("Comercios importados: "+getInfoComercios().getRegistrosBase().size());
        }
        else{
            lblProgreso.setBackground(Color.red);
        lblProgreso.setText("Error!!! Actualice el archivo e intentelo nuevamente");
        }


    }

    /**
     * @return the tipoLiquidacion
     */
    public int[] getTipoLiquidacion() {
        return tipoLiquidacion;
    }

    /**
     * @param tipoLiquidacion the tipoLiquidacion to set
     */
    public void setTipoLiquidacion(int[] tipoLiquidacion) {
        this.tipoLiquidacion = tipoLiquidacion;
    }

    
}
