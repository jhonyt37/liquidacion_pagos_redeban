/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.redeban.bd;

import java.io.IOException;
import java.security.InvalidKeyException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author javie
*/
public class ConexionSQLite {

    static org.apache.log4j.Logger logger = org.apache.log4j.Logger
            .getLogger(ConexionSQLite.class);

    private static Connection connection = null;
    private static final ConfiguracionGral CM = new ConfiguracionGral();

    private ConexionSQLite(){
        // constructor para acceso a varibables y metodos estaticos
    }
    /**
     * Método tipo singleton para verificar que la clase no se encuentre
     * instanciada previamente en memoria, de esta forma se usan los métodos
     * para obtener la conexión establecida a la BD.
     *
     * @return Retorna la instancia de la clase con todos sus métodos activos.
     * @throws java.sql.SQLException
     * @throws Exception
     */
    public static Connection connect() throws SQLException {
        try {
            if (connection == null) {
                // db parameters
                String url = "jdbc:sqlite:" + CM.getURLBDProperty();
                // create a connection to the database
                connection = DriverManager.getConnection(url);
            }

        } catch (SQLException e) {
            throw new SQLException(e);
        } catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(ConexionSQLite.class.getName()).log(Level.SEVERE, null, ex);
        }
        return connection;

    }

    /**
     * Método que se encarga de devolver la conexion establecida.
     *
     * @return Retorna el objeto con la conexión de la base de datos.
    */
    public static synchronized java.sql.Connection getDBConexion(){
        return connection;
    }

    /**
     * Método encargado de cerrar y liberar la conexión de la base de datos.
     *
     * @param conn Conexión actual de la base de datos.
    */
    public static void liberarConexion() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (java.sql.SQLException ex) {
            logger.error("Error al tratar de liberar la conexión: ", ex);
        }
    }

}

