/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.redeban.bd;

import com.rbm.cipherlightweight.security.facade.impl.CipherLightWeightFacade;
import java.security.InvalidKeyException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import org.apache.log4j.Logger;

/**
 *
 * @author javie
 */
public class ConfiguracionGral {

    private static final String CONFIGURATION_PATH = "./recursos/config/configuracion.properties";
    static Logger logger = org.apache.log4j.Logger.getLogger(ConfiguracionGral.class);
    private static  CipherLightWeightFacade cipherFacade = new CipherLightWeightFacade();

    private static final String URL_BASE_DATOS = "URL.BASE.DATOS";
    private static final String IP_SERVIDOR_FTP = "IP.SERVIDOR.SFTP";
    private static final String USUARIO_SERVIDOR_FTP = "USUARIO.SERVIDOR.SFTP";
    private static final String PASSWORD_SERVIDOR_FTP = "PASSWORD.SERVIDOR.SFTP";
    private static final String PUERTO_SERVIDOR_FTP = "PUERTO.SERVIDOR.SFTP";
    private static final String CARPETA_SERVIDOR_FTP = "CARPETA.SERVIDOR.FTP";
    private static final String CARPETA_DESCARGA_LOCAL = "CARPETA.DESCARGA.LOCAL";
    /**
     * Método encargado de cargar el archivo de configuraciones. Si no existe
     * archivo solicitará información inicial de configuración.
     *
     * @return Retorna contenido del archivo de configuraciones.
     */
    public java.util.Properties getLoadGeneralProperties() {
        java.util.Properties properties = new java.util.Properties();
        try {
            properties.load(new java.io.FileInputStream(CONFIGURATION_PATH));
        } catch (java.io.FileNotFoundException e) {
            logger.error("No se encuentra el archivo de configuración: ", e);
        } catch (java.io.IOException e) {
            logger.error("Error al cargar el archivo de configuración: ", e);
        }
        return properties;
    }

    /**
     * Método que permite obtener desde el archivo de configuración la URL
     * del archivo de Base de Datos.
     *
     * @return Retorna ruta del archivo de base de datos.
     */
    public String getURLBDProperty() throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        return cipherFacade.getValueDecipher(getLoadGeneralProperties().getProperty(URL_BASE_DATOS));
    }
    
    public String getIPServidorFTPProperty() throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        return cipherFacade.getValueDecipher(getLoadGeneralProperties().getProperty(IP_SERVIDOR_FTP));
    }
    
    public Integer getPuertoServidorFTPProperty() throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        return Integer.valueOf(cipherFacade.getValueDecipher(getLoadGeneralProperties().getProperty(PUERTO_SERVIDOR_FTP)));
    }
    
    public String getUsuarioServidorFTPProperty() throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        return cipherFacade.getValueDecipher(getLoadGeneralProperties().getProperty(USUARIO_SERVIDOR_FTP));
    }
    
    public String getPasswordServidorFTPProperty() throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        return cipherFacade.getValueDecipher(getLoadGeneralProperties().getProperty(PASSWORD_SERVIDOR_FTP));
    }
    
    public String getDirectorioFTPProperty() throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        return cipherFacade.getValueDecipher(getLoadGeneralProperties().getProperty(CARPETA_SERVIDOR_FTP));
    }
    
    public String getDirectorioLocalProperty() throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
        return cipherFacade.getValueDecipher(getLoadGeneralProperties().getProperty(CARPETA_DESCARGA_LOCAL));
    }

}
