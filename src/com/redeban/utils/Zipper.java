/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.redeban.utils;

import java.io.File;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;


/**
 *
 * @author jmtoro
 */
public class Zipper {
    
    private static final String EXTENSION = "zip";


    public void pack(String filePath) throws ZipException
    {
        ZipParameters zipParameters = new ZipParameters();
        zipParameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
        zipParameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_ULTRA);
        zipParameters.setEncryptFiles(true);
        zipParameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
        zipParameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
        
        String baseFileName = filePath;
        String destinationZipFilePath = baseFileName + "." + EXTENSION;
        ZipFile zipFile = new ZipFile(destinationZipFilePath);
        zipFile.addFile(new File(filePath), zipParameters);
    }

    public void unpack(String sourceZipFilePath, String extractedZipFilePath,String pwd) throws ZipException
    {
        ZipFile zipFile = new ZipFile(sourceZipFilePath);

        if (zipFile.isEncrypted())
        {
            zipFile.setPassword(pwd);
        }

        zipFile.extractAll(extractedZipFilePath);
    }
    
    public boolean checkPassword(String sourceZipFilePath) throws ZipException {

        ZipFile zipFile = new ZipFile(sourceZipFilePath);

        return zipFile.isEncrypted();
    }
}
