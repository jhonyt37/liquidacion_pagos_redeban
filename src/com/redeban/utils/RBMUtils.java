/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.redeban.utils;

import com.redeban.ExcelSalida;
import com.redeban.tipos.InfoRegistro;
import com.redeban.LeerFicherosExcel;
import com.redeban.tipos.LlaveData;
import static com.redeban.utils.DEFINITIONS.HOJA_UNO;
import static com.redeban.utils.DEFINITIONS.USER_DIR;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author jmtoro
 */
public class RBMUtils {
    
    
    private RBMUtils(){
        // constructor para acceso a varibables y metodos estaticos
    }

    public static String replaceSpecialChars(String in) {
        return in.replaceAll("[^\\dA-Za-z]", "");
    }

    //Elimina decimales y caracteres especiales de entrada
    public static Map<LlaveData, InfoRegistro> crearMap(Set<String[]> datos, int... indexLlave) throws IOException {

        Map<LlaveData, InfoRegistro> res = new ConcurrentHashMap<>();
        if (datos != null) {
            res = new ConcurrentHashMap<>(datos.size());

            for (String[] obj : datos) {
                if (obj instanceof String[]) {

                    String[] array = obj;
                    LlaveData llave = new LlaveData(indexLlave.length);

                    for (int indice : indexLlave) {
                        String input = array[indice].replace(",00", "");
                        llave.addCampoLlave(input.replaceFirst("^0+(?!$)", ""));
                    }

                    if (null != res.put(llave, new InfoRegistro(array))) {
                        RedebanLogger.escribeLogError("registro no agregado para conciliar, ya exite " + Arrays.toString(array));
                    }

                } else {
                    RedebanLogger.escribeLogError("registro no procesado " + obj.getClass());
                }
            }
        }
        return res;
    }

    public static File seleccionArchivo(String titulo, String mensaje, String... tipo) throws IOException {
        File nombreSalida = null;
        JFileChooser chooser = new JFileChooser(System.getProperty(USER_DIR));
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                mensaje, tipo);
        chooser.setFileFilter(filter);
        chooser.setDialogTitle(titulo);
        int returnVal = chooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                nombreSalida = chooser.getSelectedFile();

                RedebanLogger.escribeLogInfo("You chose to open this file: "
                        + chooser.getSelectedFile().getAbsolutePath());

            } catch (Exception ex) {
                RedebanLogger.escribeLogError("leer archivo entrada() " + ex.getMessage());
                throw new IOException(ex);
            }
        }
        return nombreSalida;
    }
    

    
    public static String obtenerHeader(File pendientes, String hoja) throws IOException {

        String header = "";
        Set<List<String>> datos = LeerFicherosExcel.leerArchivoExcel(pendientes, hoja, 0, 1, true,null);
        Iterator<List<String>> it = datos.iterator();

        StringBuilder tmp = new StringBuilder();
        while (it.hasNext()) {
            tmp.append(String.valueOf(it.next()));
        }
        header = tmp.toString();
        return header;
    }

    public static int sizeExistente(File pendientes) throws IOException {

        int count = 0;
        
        ExcelSalida archivoOK = new ExcelSalida(pendientes,HOJA_UNO);
        count = archivoOK.leerArchivoPrevio(pendientes.getAbsolutePath(),HOJA_UNO);
        
        return count;
    }
    
        public static int sizeExistente(File pendientes,String hoja) throws IOException {

        int count = 0;
        
        ExcelSalida archivoOK = new ExcelSalida(pendientes,hoja);
        count = archivoOK.leerArchivoPrevio(pendientes.getAbsolutePath(),hoja);
        
        return count;
    }
    
    public static int sizeExistente(String previo) throws IOException {

        
        
        return new ExcelSalida().leerArchivoPrevio(previo,HOJA_UNO);
        
    }
    
    public static <T> T[] concatenar(T[] first, T[] second) {
        T[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

    
    public static String replaceSpecialChar(String input){
        
        byte[] response = input.getBytes(StandardCharsets.ISO_8859_1);
        return new String(response, StandardCharsets.ISO_8859_1);
    }


}
