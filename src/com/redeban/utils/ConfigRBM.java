/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.redeban.utils;

import static com.redeban.utils.DEFINITIONS.USER_DIR;
import java.io.IOException;

/**
 *
 * @author jmtoro
 */
public class ConfigRBM {
    
    private static final String CONFIGURATION_PATH = "./recursos/config/configuracion.properties";
    
    private static final String CARPETA_SALIDA_SITA = "CARPETA.SALIDA.SITA";
    private static final String CARPETA_SALIDA_EXTRACTO = "CARPETA.SALIDA.EXTRACTO";
    private static final String CARPETA_SALIDA_CONCILIACION = "CARPETA.SALIDA.CONCILIACION";
    
    private static final String NOMBRE_SALIDA_SITA = "NOMBRE.SALIDA.SITA";
    private static final String NOMBRE_SALIDA_EXTRACTO = "NOMBRE.SALIDA.EXTRACTO";
    private static final String NOMBRE_SALIDA_CONCILIACION = "NOMBRE.SALIDA.CONCILIACION";
    private static final String CUENTAS_ENTIDAD_OK = "CUENTAS.ENTIDAD.OK";
    
    private static final String CARPETA_CONCILIACION_PENDIENTES_SITA = "CARPETA.CONCILIACION.PENDIENTES.SITA";
    private static final String CARPETA_CONCILIACION_PENDIENTES_ENTIDAD = "CARPETA.CONCILIACION.PENDIENTES.ENTIDAD";
    
    private static final String NOMBRE_PENDIENTES_SITA = "NOMBRE.PENDIENTES.SITA";
    private static final String NOMBRE_DIFERENCIAS_SITA = "NOMBRE.DIFERENCIAS.SITA";
    private static final String NOMBRE_PENDIENTES_ENTIDAD = "NOMBRE.PENDIENTES.ENTIDAD";
    private static final String NOMBRE_DIFERENCIAS_ENTIDAD = "NOMBRE.DIFERENCIAS.ENTIDAD";
    
    private static final String FACTOR_MONTOS_ENTIDAD = "FACTOR.MONTOS.ENTIDAD";
    
    private static final String CARPETA_LIQUIDACION = "CARPETA.LIQUIDACION";
    private static final String CARPETA_ENTRADA_LIQUIDACION = "CARPETA.ENTRADA.LIQUIDACION";
    private static final String CARPETA_INFO_COMERCIOS = "CARPETA.INFO.COMERCIOS";
    private static final String NOMBRE_INFO_COMERCIOS = "NOMBRE.INFO.COMERCIOS";
    private static final String NOMBRE_HOJA_INFO_COMERCIOS = "NOMBRE.HOJA.INFO.COMERCIOS";
    private static final String NOMBRE_SALIDA_LIQUIDACION = "NOMBRE.SALIDA.LIQUIDACION";
    private static final String NOMBRE_SALIDA_LIQUIDACION_INDIVIDUAL = "NOMBRE.SALIDA.LIQUIDACION.INDIVIDUAL";
    
    private static final String NOMBRE_ENTRADA_LIQUIDACION = "NOMBRE.ENTRADA.LIQUIDACION";
    
    private static final String COMERCIOS_COLUMNAS_VALIDA_VACIOS = "COMERCIOS.COLUMNAS.VALIDA.VACIOS";
    
    private static final String COMERCIOS_COLUMNA_PRODUCTO = "COMERCIOS.COLUMNA.PRODUCTO";
    private static final String COMERCIOS_COLUMNA_PRODUCTO_NO_VACIO = "COMERCIOS.COLUMNA.PRODUCTO.NO.VACIO";
    
    private static final String COMERCIOS_COLUMNAS_PRODUCTO_VALIDA_CONTENIDO = "COMERCIOS.COLUMNAS.PRODUCTO.VALIDA.CONTENIDO";
    
    
    
    
    
    
    
    
    
    private ConfigRBM(){
        //contructor vacio, unico uso estatico de propiedades
    }
         
 


 
    /**
     * Método encargado de cargar el archivo de configuraciones. Si no existe
     * archivo solicitará información inicial de configuración.
     *
     * @return Retorna contenido del archivo de configuraciones.
     */
    public static java.util.Properties  getLoadGeneralProperties() throws IOException{
        java.util.Properties properties = new java.util.Properties();
        try {
            properties.load(new java.io.FileInputStream(CONFIGURATION_PATH));
        } catch (java.io.FileNotFoundException e) {
            RedebanLogger.escribeLogError("No se encuentra el archivo de configuración: "+ e);
        }
        return properties;
    }

    /**
     * Método que permite obtener desde el archivo de configuración la URL
     * del archivo de Base de Datos.
     *
     * @return Retorna ruta del archivo de base de datos.
     */
    public static String getRutaSita() throws  IOException {
        String rutaOut= getLoadGeneralProperties().getProperty(CARPETA_SALIDA_SITA);
        if(rutaOut.startsWith("/"))
            rutaOut = getRutaProyecto()+rutaOut;
        rutaOut = rutaOut.replaceAll("[/]", "\\\\");
            
        return rutaOut;
    }
    
    public static String getRutaEntidad() throws IOException {
        String rutaOut= getLoadGeneralProperties().getProperty(CARPETA_SALIDA_EXTRACTO);
        if(rutaOut.startsWith("/"))
            rutaOut = getRutaProyecto()+rutaOut;
        rutaOut = rutaOut.replaceAll("[/]", "\\\\");
        return rutaOut;
    }
    
    public static String getRutaConcilia() throws  IOException {
        
        String rutaOut = getLoadGeneralProperties().getProperty(CARPETA_SALIDA_CONCILIACION);
        if (rutaOut.startsWith("/")) {
            rutaOut = getRutaProyecto() + rutaOut;
        }
        rutaOut = rutaOut.replaceAll("[/]", "\\\\");
        return rutaOut;
    }

    public static String getNombreSita() throws IOException {
        return getLoadGeneralProperties().getProperty(NOMBRE_SALIDA_SITA);
    }
    
    public static String getNombreEntidad() throws IOException {
        return getLoadGeneralProperties().getProperty(NOMBRE_SALIDA_EXTRACTO);
    }
    
    public static String getNombreConciliacion() throws IOException {
        return getLoadGeneralProperties().getProperty(NOMBRE_SALIDA_CONCILIACION);
    }
    
    public static String getRutaProyecto(){
        return System.getProperty(USER_DIR);
    }
    
    public static String getCuentasBancoOK() throws IOException {
        return getLoadGeneralProperties().getProperty(CUENTAS_ENTIDAD_OK);
    }
    
    
    public static String getRutaPendientesEntidad() throws  IOException {
        String rutaOut= getLoadGeneralProperties().getProperty(CARPETA_CONCILIACION_PENDIENTES_ENTIDAD);
        if(rutaOut.startsWith("/"))
            rutaOut = getRutaProyecto()+rutaOut;
        rutaOut = rutaOut.replaceAll("[/]", "\\\\");
        return rutaOut;
    }
    
    public static String getRutaPendientesSita() throws  IOException {
        String rutaOut= getLoadGeneralProperties().getProperty(CARPETA_CONCILIACION_PENDIENTES_SITA);
        if(rutaOut.startsWith("/"))
            rutaOut = getRutaProyecto()+rutaOut;
        rutaOut = rutaOut.replaceAll("[/]", "\\\\");
        return rutaOut;
    }
    
    public static String getNombreDiferenciasSita() throws IOException {
        return getLoadGeneralProperties().getProperty(NOMBRE_DIFERENCIAS_SITA);
    }
    
    public static String getNombrePendientesSita() throws IOException {
        return getLoadGeneralProperties().getProperty(NOMBRE_PENDIENTES_SITA);
    }
    
    public static String getNombreDiferenciasEntidad() throws IOException {
        return getLoadGeneralProperties().getProperty(NOMBRE_DIFERENCIAS_ENTIDAD);
    }
    
    public static String getNombrePendientesEntidad() throws IOException {
        return getLoadGeneralProperties().getProperty(NOMBRE_PENDIENTES_ENTIDAD);
    }
    
    public static String getFactorMontosEntidad()throws IOException {
        return getLoadGeneralProperties().getProperty(FACTOR_MONTOS_ENTIDAD);
    }
    
    /**
     * Método que permite obtener desde el archivo de configuración la URL
     * del archivo de Base de Datos.
     *
     * @return Retorna ruta del archivo de base de datos.
     */
    public static String getRutaLiquidacion() throws  IOException {
        String rutaOut= getLoadGeneralProperties().getProperty(CARPETA_LIQUIDACION);
        if(rutaOut.startsWith("/"))
            rutaOut = getRutaProyecto()+rutaOut;
        rutaOut = rutaOut.replaceAll("[/]", "\\\\");
            
        return rutaOut;
    }
    
    /**
     * Método que permite obtener desde el archivo de configuración la URL
     * del archivo de Base de Datos.
     *
     * @return Retorna ruta del archivo de base de datos.
     */
    public static String getRutaEntradaLiquidacion() throws  IOException {
        String rutaOut= getLoadGeneralProperties().getProperty(CARPETA_ENTRADA_LIQUIDACION);
        if(rutaOut.startsWith("/"))
            rutaOut = getRutaProyecto()+rutaOut;
        rutaOut = rutaOut.replaceAll("[/]", "\\\\");
            
        return rutaOut;
    }
    
    /**
     * Método que permite obtener desde el archivo de configuración la URL
     * del archivo de Base de Datos.
     *
     * @return Retorna ruta del archivo de base de datos.
     */
    public static String getRutaInfoComercios() throws  IOException {
        String rutaOut= getLoadGeneralProperties().getProperty(CARPETA_INFO_COMERCIOS);
        if(rutaOut.startsWith("/"))
            rutaOut = getRutaProyecto()+rutaOut;
        rutaOut = rutaOut.replaceAll("[/]", "\\\\");
            
        return rutaOut;
    }
        
    public static String getNombreInfoComercios() throws IOException {
        return getLoadGeneralProperties().getProperty(NOMBRE_INFO_COMERCIOS);
    }
    
    public static String getNombreHojaInfoComercios() throws IOException {
        return getLoadGeneralProperties().getProperty(NOMBRE_HOJA_INFO_COMERCIOS);
    }
    
    
    public static String getNombreSalidaLiquidacion() throws IOException {
        return getLoadGeneralProperties().getProperty(NOMBRE_SALIDA_LIQUIDACION);
    }
    public static String getNombreSalidaLiquidacionIndividual() throws IOException {
        return getLoadGeneralProperties().getProperty(NOMBRE_SALIDA_LIQUIDACION_INDIVIDUAL);
    }
    
    public static String getNombreEntradaLiquidacion() throws IOException {
        return getLoadGeneralProperties().getProperty(NOMBRE_ENTRADA_LIQUIDACION);
    }

    public static String getComerciosColumnaValidaVacios() throws IOException {
        return getLoadGeneralProperties().getProperty(COMERCIOS_COLUMNAS_VALIDA_VACIOS);
    }
        
    public static String getComerciosColumnaProducto() throws IOException {
        return getLoadGeneralProperties().getProperty(COMERCIOS_COLUMNA_PRODUCTO);
    }
    
    public static String getComerciosColumnaProductoNoVacio() throws IOException {
        return getLoadGeneralProperties().getProperty(COMERCIOS_COLUMNA_PRODUCTO_NO_VACIO);
    }

     public static String getComerciosColumnasProductoContenido() throws IOException {
        return getLoadGeneralProperties().getProperty(COMERCIOS_COLUMNAS_PRODUCTO_VALIDA_CONTENIDO);
    }
    
    
}

