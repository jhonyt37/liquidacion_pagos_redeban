/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.redeban.utils;

/**
 *
 * @author jmtoro
 */
public class DEFINITIONS {
    public static final String USER_DIR ="user.dir";
    public static final String XLSX_EXT =".xlsx";
    public static final String PROYECT_NAME ="Liquidacion_Pagos_RBM.jar";
    
    public static final int SITA_PROCESO_NUEVO =0;
    public static final int SITA_PROCESO_PENDIENTES =1;
    public static final int SITA_PROCESO_GUARDAR_TEMPORAL =2;
    
    public static final int BANCO_PROCESO_NUEVO =3;
    public static final int BANCO_PROCESO_PENDIENTES =4;
    public static final int BANCO_PROCESO_GUARDAR_TEMPORAL =5;
    
    
    public static final int CONCILIACION_PROCESO =6;
    public static final int LIQUIDACION_PROCESO =7;
    public static final int DISPERSION_PROCESO =8;
    
    public static final int QR_PROCESO =9;
    
    public static final int LIQUIDACION_PROCESO_INFO_COMERCIOS=0;
    
    public static final String HOJA_UNO="Hoja1";
    
    public static final String ESTADO_NO_LIQUIDADO ="No Liquidado";
    public static final String ETIQUETA_ESTADO ="estado";
    public static final String ETIQUETA_MEMORIA_CERO ="En memoria: 0";
    public static final String ETIQUETA_SIN_PENDIENTES= "No hay pendientes";
    public static final String ETIQUETA_PENDIENTES= "Registros pendientes: ";
    public static final String ETIQUETA_ERROR_SITA= "Error Archivo SITA";
    
     
    
    
    private DEFINITIONS(){
        // constructor para acceso a varibables y metodos estaticos
    }
    
}
