package com.redeban.utils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import static com.redeban.utils.DEFINITIONS.PROYECT_NAME;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.JTextArea;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author jhony
 */
public class RedebanLogger {

    /**
     * @return the textLog
     */
    
    private RedebanLogger(){
        /*//contructor local*/
    }
    
    public static JTextArea getTextLog() {
        return textLog;
    }

    /**
     * @param aTextLog the textLog to set
     */
    public static void setTextLog(JTextArea aTextLog) {
        textLog = aTextLog;
    }

    private static JTextArea textLog;
    private static Logger log;
    private static final String FORMATOFECHA = "yyyy-MM-dd HH:mm:ss";
    
    /**
     *
     * @param txt
     */
    public static void setTextArea( JTextArea txt) {
        setTextLog(txt); 
    }
    
    /**
     * metodo para leer archivo de configuracion dle logger
     */
    public static void configFile (){
        log = Logger.getLogger(PROYECT_NAME);

        String log4jConfPath = System.getProperty("user.dir") + "/recursos/config/log.properties";

        if (new File(log4jConfPath).exists()) {

            try {
                PropertyConfigurator.configure(log4jConfPath);
            } catch (Exception ex) {
                escribeLogErrorInterno(ex.getMessage());
            }
        } else {
            escribeLogErrorInterno("error obteniendo archivo config");
        }

        BasicConfigurator.configure();
    }

    public static void escribeLogInfo(String mensaje) {
        String timeStamp = new SimpleDateFormat(FORMATOFECHA).format(Calendar.getInstance().getTime());
        
        if(getTextLog()!=null)
            getTextLog().append(timeStamp + ": "+ mensaje + "\r\n");
        log.debug(mensaje);
    }
    
    public static  void escribeLogTrace(String mensaje) {
        log.trace(mensaje);
    }
    
    public static  void escribeLogError(String mensaje) {
        String timeStamp = new SimpleDateFormat(FORMATOFECHA).format(Calendar.getInstance().getTime());
        
        if (getTextLog() != null) {
            getTextLog().append(timeStamp + ": " + mensaje + "\r\n");
            getTextLog().setCaretPosition(getTextLog().getDocument().getLength());
        }
        log.error(mensaje);
    }
    
    public static void escribeLogErrorInterno(String mensaje) {
        String timeStamp = new SimpleDateFormat(FORMATOFECHA).format(Calendar.getInstance().getTime());
        
        log.error(timeStamp + ": " + mensaje + "\r\n");
    }
    
    public static void escribeLogFatal(String mensaje) {
        String timeStamp = new SimpleDateFormat(FORMATOFECHA).format(Calendar.getInstance().getTime());
        if (getTextLog() != null) {
            getTextLog().append(timeStamp + ": " + mensaje + "\r\n");
            getTextLog().setCaretPosition(getTextLog().getDocument().getLength());
        }
        
        log.fatal(mensaje);
    }

}
