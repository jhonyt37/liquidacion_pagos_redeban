package com.redeban.utils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.swing.JLabel;

/**
 *
 * @author jmtoro
 */
public class Archivos {
    public static final int REGISTRO_OK=0;
    public static final int REGISTRO_ALERTA=1;
    public static final int REGISTRO_DESCARTADO=2;
    private static JLabel lblOut;
    private static char charSeparator;
    

    
    public static void setLabelOut(JLabel lbl){
        lblOut = lbl;
        
    }

    public static JLabel getLabelOut() {
        return lblOut;
    }
    

    
    public static void viewData(String msj){
        if(lblOut!=null)
            lblOut.setText(msj);
    }

    public static float countLinesFile(File rutaArchivo) throws IOException {
        float fileSize = 0;

        try (
                BufferedReader br = new BufferedReader(new FileReader(rutaArchivo));) {
            String res = null;
            while ((res = br.readLine()) != null) {
                fileSize++;
            }

        } catch (IOException ex) {
            RedebanLogger.escribeLogError(ex.getMessage());
        }
        return fileSize;
    }
    
    

    /**
     * @return the charSeparator
     */
    public static char getCharSeparator() {
        return charSeparator;
    }

    /**
     * @param aCharSeparator the charSeparator to set
     */
    public static void setCharSeparator(char aCharSeparator) {
        charSeparator = aCharSeparator;
    }
    
    public static boolean deleteFile(String rutaArchivo) {
        boolean res = false;
        File file = new File(rutaArchivo);

        if (file.exists()) {// si el archivo existe se borra
            Path pat = Paths.get(file.getAbsolutePath());
            try {
                res = Files.deleteIfExists(pat);
            } catch (IOException ex) {
                RedebanLogger.escribeLogError(ex.getLocalizedMessage());
            }
        }
        return res;
    }

}
