package com.redeban;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.redeban.tipos.InfoBase;
import com.redeban.tipos.InfoRegistro;
import com.redeban.tipos.LlaveData;
import com.redeban.utils.DEFINITIONS;
import com.redeban.utils.RBMUtils;
import com.redeban.utils.RedebanLogger;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.swing.JLabel;

/**
 *
 * @author jmtoro
 */
public class ProcesoEntradaConciliados extends Thread {

    /**
     * @return the infoConciliados
     */
    public InfoBase getInfoConciliados() {
        return infoConciliados;
    }

    /**
     * @param infoConciliados the infoConciliados to set
     */
    public void setInfoConciliados(InfoBase infoConciliados) {
        this.infoConciliados = infoConciliados;
    }

    
    private File entradaFile;
    
    private InfoBase infoConciliados;

    JLabel lblProgreso;

    public ProcesoEntradaConciliados( File f, JLabel lbl) {
        this.entradaFile = f;
        this.lblProgreso = lbl;
    }
    
    
    @Override
    public void run() {
        
        setInfoConciliados(new InfoBase());
        
        getInfoConciliados().setMaxEstados(1);
        getInfoConciliados().setCharSeparator(',');
        getInfoConciliados().defineCamposLlaveBase(31,30,26,27,35);
        getInfoConciliados().setRegistrosBase(new ConcurrentHashMap<>());
        
        try {
            
            String hoja = DEFINITIONS.HOJA_UNO;
            
            String campos = RBMUtils.obtenerHeader(entradaFile, hoja).replaceAll("[^a-zA-Z0-9;, /]", " ").toUpperCase();
            getInfoConciliados().setListaCampos(campos.split(","));
            
            getInfoConciliados().incluirPendientesDetalle(entradaFile,
                    hoja,
                    1,
                    lblProgreso);
            
            
        } catch (IOException ex) {
            RedebanLogger.escribeLogError(ex.getMessage());
            
        }
        int contador=0;
        for (Map.Entry<LlaveData, InfoRegistro> entry : getInfoConciliados().getRegistrosBase().entrySet()) {
            if(entry.getValue().getRegistro()[entry.getValue().getRegistro().length-1].equals(DEFINITIONS.ESTADO_NO_LIQUIDADO)){
                contador++;
            }
        }
        lblProgreso.setBackground(Color.green);
        lblProgreso.setText("Registros para Liquidar: "+contador+" / "+getInfoConciliados().getRegistrosBase().size());
        
    }
    
}
