/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.redeban;

import com.redeban.utils.RedebanLogger;
import com.redeban.tipos.LlaveData;
import com.redeban.tipos.InfoRegistro;
import com.redeban.utils.Archivos;
import static com.redeban.utils.Archivos.setLabelOut;
import com.redeban.utils.ConfigRBM;
import static com.redeban.utils.DEFINITIONS.ESTADO_NO_LIQUIDADO;
import static com.redeban.utils.DEFINITIONS.HOJA_UNO;
import static com.redeban.utils.DEFINITIONS.XLSX_EXT;
import com.redeban.utils.RBMUtils;
import com.redeban.windows.MainWondow;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author jmtoro
 */
public class Conciliacion extends Archivos{


    /**
     * @return the historico
     */
    public boolean isHistorico() {
        return historico;
    }

    /**
     * @param historico the historico to set
     */
    public void setHistorico(boolean historico) {
        this.historico = historico;
    }

    
    
    private static final String RUTA_SALIDA_DIFERNECIAS= "Diferencias\\";
    private static final String NOMBRE_SALIDA ="";
    private String rutaSalida;
    private String nombreSalida;
    private boolean historico;
    Set<Map.Entry<LlaveData,InfoRegistro>> setSita;
    Set<InfoRegistro> setEntidad;
    
    Map<LlaveData,InfoRegistro> salidaMap;
    int salidasOK;
    int diferenciasRbm;
    int diferenciasEntidad;
    int previos;
    
    ArrayList<Integer[]> columnasSalida;
    private String headerConcilia;
    private String headerSita;
    private String headerEntidad;

    public void init(JLabel lbl){
        setLabelOut(lbl);
        setSita = new HashSet<>();
        setEntidad = new HashSet<>();
        salidaMap = new ConcurrentHashMap<>();
        columnasSalida = new ArrayList<>();
    }
    
    public void setSalida(String n, String r) {
        nombreSalida = n;
        rutaSalida = r;
    }
    
    
    
    public void addColsSalida(int input, int columna) {
        columnasSalida.add(new Integer[] { input, columna });
    }
    


    public int getCountPendientesSita() {
        return setSita.size();
    }

    public int getCountPendientesEntidad() {
        return setEntidad.size();
    }
    
    public InfoRegistro getRegistroSalida(String[] sita, String[] entidad) throws IOException{
        InfoRegistro lista = new InfoRegistro(columnasSalida.size());
        String res="";
        for (Integer[] entrada : columnasSalida) {

            if (entrada[0] == 0) {
                res = sita[entrada[1]];
            } else {
                res = entidad[entrada[1]];
            }

            lista.addCampo(res);
        }
        return lista;
    }

    public void ejecutarProceso(Set<String[]> sita, Set<String[]> extracto) throws IOException {

        setSita = new HashSet<>();
        setEntidad = new HashSet<>();
        viewData("Inicio Conciliacion:");
        previos=0;
        //banco
        Map<LlaveData, InfoRegistro>  entidadMap = RBMUtils.crearMap(extracto, 
                ExtractoInfo.COLUMNA_CAMPO_CU, 
                ExtractoInfo.COLUMNA_CAMPO_ULTIMOSCUATRO,
                ExtractoInfo.COLUMNA_CAMPO_APROBACION,
                ExtractoInfo.COLUMNA_CAMPO_TOTAL);
        //sita
        Map<LlaveData, InfoRegistro> sitaMap  = RBMUtils.crearMap(sita, 
                SitaInfo.COLUMNA_CAMPO_CU,
                SitaInfo.COLUMNA_CAMPO_ULTIMOSCUATRO,
                SitaInfo.COLUMNA_CAMPO_APROBACION,
                SitaInfo.COLUMNA_CAMPO_TOTAL);

        salidaMap = new ConcurrentHashMap<>();
        
        float lenTotal = entidadMap.size();
        if(lenTotal ==0)
            throw new IOException("  Error en longitud de datos de entrada, por favor verifique!!!");
        
        int index=0;
        for (Object obj : entidadMap.entrySet()) {
            viewData("Conciliacion... "+(index/lenTotal)+ " %");
            Map.Entry<LlaveData, InfoRegistro> entryEntidad = (Map.Entry<LlaveData, InfoRegistro>) obj;
            RedebanLogger.escribeLogInfo("Buscando: "+Arrays.toString(entryEntidad.getKey().getCampos()));
            Object tmp;
            if (null != (tmp=sitaMap.get(entryEntidad.getKey()))) {
                InfoRegistro entrySita = (InfoRegistro) tmp;
               
                InfoRegistro registro = getRegistroSalida(entrySita.getRegistro(), entryEntidad.getValue().getRegistro());

                salidaMap.put(entryEntidad.getKey(),registro);
                sitaMap.remove(entryEntidad.getKey());
                RedebanLogger.escribeLogInfo("Registro OK");
                
            } else {
                setEntidad.add(entryEntidad.getValue());
                RedebanLogger.escribeLogError("Registro no encontrado");
            }
        }

        if (!sitaMap.isEmpty()) {
            setSita.addAll(sitaMap.entrySet());
        }

        RedebanLogger.escribeLogInfo("Datos sin conciliar Sita      : " + setSita.size());
        RedebanLogger.escribeLogInfo("Datos sin conciliar Entidad   : " + setEntidad.size());
        RedebanLogger.escribeLogInfo("Datos conciliados             : " + salidaMap.size());
        RedebanLogger.escribeLogInfo("Datos previos de conciliacion : " + previos);

        generarArchivos();
        MainWondow.mostrarMensaje("Conciliacion","Resultado conciliacion: "
                    + "\n Datos sin conciliar Sita      : " + setSita.size()
                    + "\n Datos sin conciliar Entidad   : " + setEntidad.size()
                    + "\n Datos conciliados             : " + salidaMap.size()
                    + "\n Datos previos de conciliacion : " + previos
                
                    + "\n\n Para Consultar detalle vaya a   : " + rutaSalida,1);
        
    }

    
    public void generaArchivoSalida(Set<String []> input,String tituloMsj, String... header) throws IOException{
        int diferencias=0;
        try {
            ExcelSalida archivoDiferencias = new ExcelSalida(input.size(), nombreSalida , rutaSalida , HOJA_UNO, getLabelOut());
            for (String[] obj : input) {
                archivoDiferencias.addMatrizData((Object[])obj);
                diferencias++;
            }
            if (diferencias > 0 ){
                archivoDiferencias.crearExcelsalida(false,(Object[])header);
                MainWondow.mostrarMensaje("Almacenado Pendientes "+tituloMsj,"Resultado almacenar: "
                    + "\n Datos almacenados para conciliar : " + diferencias
                    + "\n\n Para Consultar detalle vaya a   : " + rutaSalida,1);
                
            }
           
        } catch (IOException ex) {
            throw new IOException(ex.getMessage());
        }
        
    }
    
    
    public void generarArchivos() throws IOException {
        
        try {

            String nombreDiferenciasEntidad = ConfigRBM.getNombreDiferenciasEntidad();
            String nombreDiferenciasSita = ConfigRBM.getNombreDiferenciasSita();
            ExcelSalida archivoOK = new ExcelSalida(salidaMap.size(), nombreSalida + NOMBRE_SALIDA, rutaSalida , HOJA_UNO, getLabelOut());
            ExcelSalida archivoDiferenciasRbm = new ExcelSalida(setSita.size(), nombreDiferenciasSita, rutaSalida + RUTA_SALIDA_DIFERNECIAS, HOJA_UNO, getLabelOut());
            ExcelSalida archivoDiferenciasEntidad = new ExcelSalida(setEntidad.size(), nombreDiferenciasEntidad, rutaSalida + RUTA_SALIDA_DIFERNECIAS, HOJA_UNO, getLabelOut());

            salidasOK = diferenciasEntidad = diferenciasRbm = 0;
            for (Object obj : setSita) {
                Map.Entry<LlaveData,InfoRegistro> entry = (Map.Entry) obj;
                
                archivoDiferenciasRbm.addMatrizData((Object[])entry.getValue().getRegistro());
                diferenciasRbm++;

            }
            for (Object obj : setEntidad) {
                InfoRegistro entry = (InfoRegistro) obj;
                
                archivoDiferenciasEntidad.addMatrizData((Object[])entry.getRegistro());
                diferenciasEntidad++;
            }

            for (Object obj : salidaMap.entrySet()) {
                Map.Entry<LlaveData,InfoRegistro> entry = (Map.Entry) obj;
                archivoOK.addMatrizData(entry.getValue().getRegistro(),ESTADO_NO_LIQUIDADO);
                salidasOK++;
            }
            
                archivoDiferenciasEntidad.crearExcelsalida(false, (headerEntidad != null) ? headerEntidad.split(";") : "");
            
            
                archivoDiferenciasRbm.crearExcelsalida(false, (headerSita != null) ? headerSita.split(";") : "");
            
            if (diferenciasRbm > 0 || diferenciasEntidad > 0) {
                int opc = JOptionPane.showConfirmDialog(null,
                        "Existen registros no Conciliados"
                        + "\n\n Por favor verifique la entrada y ejecute nuevamente el proceso"
                        + "\n\n\n Desea continuar el proceso con estas inconsistencias?",
                        "Error Inconsistencias",
                        JOptionPane.OK_CANCEL_OPTION);
                if (opc != 0) {

                    throw new IOException("Inconsistencias encontradas, verique entrada!!!. "
                            + "\n\nPara ver información detallada de las inconsistencias consulte:"
                            + "\n\n " + rutaSalida + RUTA_SALIDA_DIFERNECIAS);
                }
            }
            
            previos = RBMUtils.sizeExistente(rutaSalida+nombreSalida + NOMBRE_SALIDA+XLSX_EXT);

            archivoOK.crearExcelsalida(this.isHistorico(),(headerConcilia!=null)?headerConcilia.split(";"):"","Estado");
           
        } catch (IOException ex) {
            throw new IOException(ex.getMessage());
        }
    }
    
    /**
     * @return the headerConcilia
     */
    public String getHeaderConcilia() {
        return headerConcilia;
    }

    /**
     * @param aHeaderConcilia the headerConcilia to set
     */
    public void setHeaderConcilia(String aHeaderConcilia) {
        headerConcilia = aHeaderConcilia;
    }

    /**
     * @return the headerSita
     */
    public String getHeaderSita() {
        return headerSita;
    }

    /**
     * @param aHeaderSita the headerSita to set
     */
    public void setHeaderSita(String aHeaderSita) {
        headerSita = aHeaderSita;
    }

    /**
     * @return the headerEntidad
     */
    public String getHeaderEntidad() {
        return headerEntidad;
    }

    /**
     * @param aHeaderEntidad the headerEntidad to set
     */
    public void setHeaderEntidad(String aHeaderEntidad) {
        headerEntidad = aHeaderEntidad;
    }
}