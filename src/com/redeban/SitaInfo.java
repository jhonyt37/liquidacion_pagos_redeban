/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.redeban;

import com.redeban.tipos.InfoBase;
import com.redeban.tipos.LlaveData;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author jmtoro
 */
public class SitaInfo extends InfoBase{

    /**
     * @return the MAX_ESTADOS
     */
    public static int getMAXESTADOS() {
        return MAX_ESTADOS;
    }

    public static final int COLUMNA_INICIAL = 0;
    
    public static final int COLUMNA_CAMPO_FECHA_TXN = 0;
    public static final int COLUMNA_CAMPO_FECHA_POSTEO = 1;
    public static final int COLUMNA_CAMPO_HORA_TXN = 2;
    
    public static final int COLUMNA_CAMPO_BIN = 3;
    public static final int COLUMNA_CAMPO_ULTIMOSCUATRO = 4;
    
    public static final int COLUMNA_CAMPO_FIID = 5;
    public static final int COLUMNA_CAMPO_AUTORIZADOR = 6;
    public static final int COLUMNA_CAMPO_CU = 7;
    public static final int COLUMNA_CAMPO_TERMINAL = 8;
    
    public static final int COLUMNA_CAMPO_NOMBRE_COMERCIO = 9;
    
    public static final int COLUMNA_CAMPO_TIPO_TRANSACCION = 10;
    public static final int COLUMNA_CAMPO_RESPUESTA = 11;
    public static final int COLUMNA_CAMPO_TOTAL = 12;
    public static final int COLUMNA_CAMPO_IVA = 13;
    public static final int COLUMNA_CAMPO_INC = 14;
    
    public static final int COLUMNA_CAMPO_ESTADO_TXN = 15;
    
    public static final int COLUMNA_CAMPO_APROBACION=16;
    public static final int COLUMNA_CAMPO_RECIBO=17;
    
    public static final int COLUMNA_FINAL = 18;
    
    public static final int TIPO_COMPRA =0;
    public static final int TIPO_REVERSO_COMPRA =1;
    public static final int TIPO_ANULACION =2;
    public static final int TIPO_REVERSO_ANULACION =3;
    private static final int MAX_ESTADOS=4;
    
    
    public SitaInfo() {
        
    }
    
   
    
    public SitaInfo(String cabecera, int size,char ch) {
        
        super.setCharSeparator(ch);
        super.setListaCampos(cabecera.split( String.valueOf(ch)));
        super.setRegistrosBase(new ConcurrentHashMap<>(size));
        super.setMaxEstados(MAX_ESTADOS);
        

    }
    
    public SitaInfo(String cabecera,char ch) {
        
        super.setCharSeparator(ch);
        super.setListaCampos(cabecera.split( String.valueOf(ch)));
        super.setRegistrosBase(new ConcurrentHashMap<>());
        super.setMaxEstados(MAX_ESTADOS);

    }
    
    
    public boolean filtarRegistros(int idColumna,String input, String valor) {

        String[] dataEntrada = input.split(String.valueOf(getCharSeparator()));

        return dataEntrada[idColumna].equals(valor);
    }
    
    public int getTipo(String descripcion){
        String [] dataEntrada = descripcion.split( String.valueOf(getCharSeparator()));
        String tipoTxn = dataEntrada[this.getIndexPosTipoTXN()];
        int tipo=-1;
        if(tipoTxn.contains("210-COMPRA")){
            tipo = TIPO_COMPRA;
        } else if (tipoTxn.contains("210-ANULA")) {
            tipo = TIPO_ANULACION;
        } else if (tipoTxn.contains("420-REVERSO") && !tipoTxn.contains("420-REVERSO ANUL")) {
            tipo = TIPO_REVERSO_COMPRA;
        } else if (tipoTxn.contains("420-REVERSO ANUL")) {
            tipo = TIPO_REVERSO_ANULACION;
        }
        return tipo;
    }
    /**
     *
     * @param llave
     * @param valor
     * @return
     * @throws IOException
     */
    @Override
    public int agegaRegistroBase(LlaveData llave, String valor) {

        int tipo=0;
        if (-1 == (tipo = getTipo(valor))) {
            return 2;
        }
        return agegaRegistroBaseFinal(llave,valor,tipo);
    }
    
}
