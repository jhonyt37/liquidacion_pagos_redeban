/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.redeban.tipos;

/**
 *
 * @author jmtoro
 */
public class LlaveData {

        private String[] campos;
        //esta variable controla 2 o mas transacciones con mismos datos de tarjeta, diferete aprobacion y recibo en ceros, por defecto se va en ceros.
        private int idAux;
        
    
        /**
     * @param size
     */
    public  LlaveData(int size) {
        campos = new String[size];
        idAux=0;
    }
    /**
     * @return the campos
     */
    public String[] getCampos() {
        return campos;
    }

    /**
     * @param campos the campos to set
     */
    public void setCampos(String[] campos) {
        int i=0;
        for(String in:campos){
        this.campos[i] = in;
        i++;
    }
    }
    
        /**
     * @param campos the campos to set
     */
    public void addCampoLlave(String nuevaLlave) {
        int i = 0;

        for (String in : this.campos) {
            if (in == null || in.equals("")) {
                this.campos[i] = nuevaLlave;
                break;
            }
            i++;
        }
    }
    

    public LlaveData(String... cmp) {
        this.campos = cmp;
        idAux=0;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof LlaveData)) {
            return false;
        }

        LlaveData otherKey = (LlaveData) object;
        for(int i = 0; i < this.getCampos().length; i++) {
            if ((otherKey.campos[i] != null && this.campos[i] != null) &&
                (!otherKey.campos[i].equals(this.campos[i]))){
                    return false;
            }
        }
        return true;
    }
    
    public void add(String str, int index) {
        
        this.campos[index] = str;
        
    }

    @Override
    public int hashCode() {
        
        int result = 17; // any prime number
        for (String campo : this.getCampos()) {
            if (campo != null ) {
                result = 31 * result + String.valueOf(campo).hashCode();
            }
        }
        if(this.idAux!=0){
            result = 31 * result + Integer.valueOf(this.idAux).hashCode();
        }
        return result;
    }

    /**
     * @return the idAux
     */
    public int getIdAux() {
        return idAux;
    }

    /**
     * @param idAux the idAux to set
     */
    public void setIdAux(int idAux) {
        this.idAux = idAux;
    }
}
