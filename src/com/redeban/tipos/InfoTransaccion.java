/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.redeban.tipos;

/**
 *
 * @author jmtoro
 */
public class InfoTransaccion {
    
    private String [] registro;
    private boolean [] estados;

    /**
     * @return the estados
     */
    public boolean[] getEstados() {
        return estados;
    }
    
    public boolean getEstadoIndex(int indice) {
        return estados[indice];
    }
    
    public void setEstadoIndex(int indice,boolean valor) {
        estados[indice] = valor;
    }

    /**
     * @param estados the estados to set
     */
    public void setEstados(boolean[] estados) {
        this.estados = estados;
    }

    public InfoTransaccion(int size){
        this.registro = new String[size];
    }    
    
    public InfoTransaccion(String[] data){
        
        this.registro = data;
        
    }
    
    public InfoTransaccion(String[] data,int cantEstados){
        this.registro = data;
        this.estados = new boolean[cantEstados];
    }
    /**
     * @return the registro
     */
    public String[] getRegistro() {
        return registro;
    }

    /**
     * @param registro the registro to set
     */
    public void setRegistro(String[] registro) {
        this.registro = registro;
    }
    
    public void addCampo(String campo) {
        for (int i = 0; i < this.registro.length; i++) {
            if ( this.registro[i] ==null) {
                this.registro[i] = campo;
                break;
            }
        }
    }
    

    
    @Override
    public boolean equals(Object object) {
        if (!(object instanceof InfoTransaccion)) {
            return false;
        }
        InfoTransaccion otherKey = (InfoTransaccion) object;

        for (int i = 0; i < this.getEstados().length; i++) {
            if (otherKey.estados[i] != this.estados[i]) {
                return false;
            }
        }

        for (int i = 0; i < this.getRegistro().length; i++) {
            if (i >= otherKey.registro.length) {
                break;
            }
            if ((otherKey.registro[i] != null && this.registro[i] != null)
                    && (!otherKey.registro[i].equals(this.registro[i]))) {
                return false;

            }
        }

        return true;
    }
    
        @Override
    public int hashCode() {
        int result = 17; // any prime number
        for (String campo : this.getRegistro()) {
            if (campo != null ) {
                result = 31 * result + String.valueOf(campo).hashCode();
            }
        }
      
        return result;
    }
   
}
