package com.redeban.tipos;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jmtoro
 */
public class RegistroExcelGenerico {


   private Object[] contenido;
   private static final int MAX_COL=200;


    public RegistroExcelGenerico() {
        this.contenido = new Object[MAX_COL];
    }
       
    public RegistroExcelGenerico(int columnas) {
        this.contenido = new Object[columnas];
    }
    
        /**
     * @return the contenido
     */
    public Object[] getContenido() {
        return contenido;
    }

    /**
     * @param contenido the contenido to set
     */
    public void setContenido(Object[] contenido) {
        this.contenido = contenido;
    }

   
}
