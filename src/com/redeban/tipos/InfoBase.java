/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.redeban.tipos;

import com.redeban.LeerFicherosExcel;
import com.redeban.utils.RedebanLogger;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.JLabel;

/**
 *
 * @author jmtoro
 */
public class InfoBase {

    /**
     * @return the maxEstados
     */
    public int getMaxEstados() {
        return maxEstados;
    }

    /**
     * @param maxEstados the maxEstados to set
     */
    public void setMaxEstados(int maxEstados) {
        this.maxEstados = maxEstados;
    }

    
    private String[] listaCampos;
    private int[] columnasLlaveBase;
    private int indexPosTipoTXN;
    
    
    private Map<LlaveData, InfoRegistro> registrosBase;
    
    private char charSeparator;
    private int maxEstados;
    

    /**
     * @return the listaCampos
     */
    public String[] getListaCampos() {
        return listaCampos;
    }

    /**
     * @param listaCampos the listaCampos to set
     */
    public void setListaCampos(String[] l) {
        this.listaCampos = l;
    }

    /**
     * @return the columnasLlaveBase
     */
    public int[] getColumnasLlaveBase() {
        return columnasLlaveBase;
    }

    /**
     * @param columnasLlaveBase the columnasLlaveBase to set
     */
    public void setColumnasLlaveBase(int[] columnasLlaveBase) {
        this.columnasLlaveBase = columnasLlaveBase;
    }

    /**
     * @return the indexPosTipoTXN
     */
    public int getIndexPosTipoTXN() {
        return indexPosTipoTXN;
    }

    /**
     * @param indexPosTipoTXN the indexPosTipoTXN to set
     */
    public void setIndexPosTipoTXN(int indexPosTipoTXN) {
        this.indexPosTipoTXN = indexPosTipoTXN;
    }

    /**
     * @return the registrosBase
     */
    public Map<LlaveData, InfoRegistro> getRegistrosBase() {
        return registrosBase;
    }

    /**
     * @param registrosBase the registrosBase to set
     */
    public void setRegistrosBase(Map<LlaveData, InfoRegistro> registrosBase) {
        this.registrosBase = registrosBase;
    }
    
    public int sizeData(){
        return this.getRegistrosBase().size();
    }

    
    /**
     * @return the charSeparator
     */
    public char getCharSeparator() {
        return charSeparator;
    }

    /**
     * @param charSeparator the charSeparator to set
     */
    public void setCharSeparator(char charSeparator) {
        this.charSeparator = charSeparator;
    }
    
    public void defineCamposLlaveBase(int... columnas) {

        this.columnasLlaveBase = columnas;

    }
    
    public LlaveData obtieneLlaveBase(String registro) {

        LlaveData llave = null;
        String[] dataEntrada = registro.split(String.valueOf(this.charSeparator));
        int i = 0;
        if (dataEntrada.length > this.columnasLlaveBase.length) {
            llave = new LlaveData(this.columnasLlaveBase.length);
            for (int index : this.columnasLlaveBase) {
                llave.add(dataEntrada[index], i);
                i++;
            }
            RedebanLogger.escribeLogInfo("llave asignada " + Arrays.toString(llave.getCampos()));
        }

        return llave;
    }
    
    public boolean contieneRegistro(LlaveData llave){
        boolean res = false;
        if(null != this.getRegistrosBase().get(llave))
            res =true;
        return res;
    }
    
    public InfoRegistro obtieneRegistro(LlaveData llave) {

        return this.getRegistrosBase().get(llave);

    }
    
    public int procesarDetalle(Set<List<String>> datos, int offset) throws IOException {

        int count = 0;
        String line = "";
        Iterator<List<String>> it = datos.iterator();
        while (it.hasNext()) {
            StringBuilder tmp = new StringBuilder();

            count++;
            if (count <= offset) {
                continue;
            }
            List<String> lista = it.next();
            for (int i = 0; i < lista.size(); i++) {
                tmp.append(lista.get(i) + this.charSeparator);
            }
            line = tmp.toString();
            LlaveData llaveBase = obtieneLlaveBase(line);
            if (1 == (agegaRegistroBase(llaveBase, line))) {
                RedebanLogger.escribeLogInfo("Registro pendiente agregado: " + line);
            }
        }
        return (count - offset);

    }
    
    public int incluirPendientesDetalle(File pendientes, String hoja, int offset, JLabel lbl) throws IOException {

        Set<List<String>> datos = LeerFicherosExcel.leerArchivoExcel(pendientes, hoja, offset, 0, true, lbl);
        return procesarDetalle(datos,offset);

    }

    public int incluirPendientes(File pendientes,String hoja, int offset) throws IOException{

        Set<List<String>> datos = LeerFicherosExcel.leerArchivoExcel(pendientes, hoja, offset, 0,true,null);
        return procesarDetalle(datos,offset);
    }
    
    public int agegaRegistroBase(LlaveData llave, String valor) {
        return agegaRegistroBaseFinal(llave,valor,0);
    }
    public int agegaRegistroBaseFinal(LlaveData llave, String valor,int tipo){
                
        int res = 0;
        String [] dataEntrada = valor.split( String.valueOf(this.charSeparator));
        InfoRegistro registro = new InfoRegistro(dataEntrada, this.getMaxEstados());
        registro.setEstadoIndex(tipo, true);
        InfoRegistro previo;
        InfoRegistro tmp;
        
        previo = tmp = getRegistrosBase().get(llave);

        if (tmp == null) {
            getRegistrosBase().put(llave, registro);
            res = 1;
        } else {

            if (!previo.equals(registro)) {
                agregarNuevoRegistroAux(llave, tipo, registro);
                res = 1;
            }
        }
            
        return res;
    }
    void agregarNuevoRegistroAux(LlaveData llaveAnt, int tipo,InfoRegistro registro){
        
        LlaveData llave=llaveAnt;
        InfoRegistro previo = getRegistrosBase().get(llave);
        int i = 0;
        boolean exist=false;
        do {
            for (int j = 0; j < this.getMaxEstados(); j++) {
                if (tipo == j && !previo.getEstadoIndex(j)) {
                    exist=true;
                    break;
                }
            }
            
            if (!exist) {
                llave.setIdAux(llave.getIdAux() + 1);
                i++;
                previo = getRegistrosBase().get(llave);
            }
        } while (i < 20 && (!exist) && previo!=null);
        if(previo==null)
            previo = registro;
        previo.setEstadoIndex(tipo, true);
        getRegistrosBase().put(llave, previo);
        
    }
    
    
}
