/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.redeban;

import com.redeban.tipos.TLVClass;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jmtoro
 */
public class TLVClassTest {
    
    public TLVClassTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getTag method, of class TLVClass.
     */
    @Test
    public void testGetTag() {
        System.out.println("getTag");
        TLVClass instance = new TLVClass();
        String expResult = "01";
        instance.setTag(expResult);
        String result = instance.getTag();
        assertEquals(expResult, result);
    }

    
    /**
     * Test of getLen method, of class TLVClass.
     */
    @Test
    public void testGetLen() {
        System.out.println("getLen");
        TLVClass instance = new TLVClass();
        String expResult = "24";
        instance.setLen(expResult);
        String result = instance.getLen();
        assertEquals(expResult, result);

    }


    /**
     * Test of getValue method, of class TLVClass.
     */
    @Test
    public void testGetValue() {
        System.out.println("getValue");
        TLVClass instance = new TLVClass();
        
        String expResult = "hola mundo";
        instance.setValue(expResult);
        
        String result = instance.getValue();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    
}
